package be.dto;

import java.io.Serializable;

public class StudentActivityMapperDTO implements Serializable {
    private Integer ID;
    private UserDTO studentDTO;
    private DidacticActivityDTO activityDTO;

    public StudentActivityMapperDTO() {
    }

    public StudentActivityMapperDTO(Integer ID, UserDTO studentDTO, DidacticActivityDTO activityDTO) {
        this.ID = ID;
        this.studentDTO = studentDTO;
        this.activityDTO = activityDTO;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public UserDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(UserDTO studentDTO) {
        this.studentDTO = studentDTO;
    }

    public DidacticActivityDTO getActivityDTO() {
        return activityDTO;
    }

    public void setActivityDTO(DidacticActivityDTO activityDTO) {
        this.activityDTO = activityDTO;
    }
}
