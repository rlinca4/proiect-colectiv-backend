package be.dto;

import be.entity.DidacticActivity;
import be.entity.types.*;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;


/**
 * The class maps a {@link DidacticActivity} object.
 *
 * @author Mara Corina
 */
public class DidacticActivityDTO implements Serializable {

    private Integer ID;
    private String courseName;
    private DidacticActivityType type;
    private Date date;
    private Time time;
    private Integer duration;
    private String room;
    private String specialization;
    private Integer group;
    private Integer semigroup;
    private WeekType weekType;
    private DayOfTheWeek dayOfTheWeek;
    private DidacticActivityStatus status;
    private DidacticActivityColor color;
    private String description;
    private UserDTO tutor;

    public DidacticActivityDTO() {
    }

    public DidacticActivityDTO(Integer ID, String courseName, DidacticActivityType type, Date date, Time time, Integer duration, String room, String specialization, Integer group, Integer semigroup,
                               WeekType weekType, DidacticActivityColor color, DayOfTheWeek dayOfTheWeek, DidacticActivityStatus status, String description, UserDTO tutor) {
        this.ID = ID;
        this.courseName = courseName;
        this.type = type;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.room = room;
        this.specialization = specialization;
        this.group = group;
        this.semigroup = semigroup;
        this.weekType = weekType;
        this.dayOfTheWeek = dayOfTheWeek;
        this.color = color;
        this.status = status;
        this.description = description;
        this.tutor = tutor;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public DidacticActivityType getType() {
        return type;
    }

    public void setType(DidacticActivityType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getSemigroup() {
        return semigroup;
    }

    public void setSemigroup(Integer semigroup) {
        this.semigroup = semigroup;
    }

    public DidacticActivityStatus getStatus() {
        return status;
    }

    public void setStatus(DidacticActivityStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserDTO getTutor() {
        return tutor;
    }

    public void setTutor(UserDTO tutor) {
        this.tutor = tutor;
    }

    public DidacticActivityColor getColor() {
        return color;
    }

    public void setColor(DidacticActivityColor color) {
        this.color = color;
    }

    public WeekType getWeekType() {
        return weekType;
    }

    public void setWeekType(WeekType weekType) {
        this.weekType = weekType;
    }

    public DayOfTheWeek getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    public void setDayOfTheWeek(DayOfTheWeek dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }
}
