package be.dto;

import be.entity.types.DayOfTheWeek;
import be.entity.types.DidacticActivityColor;
import be.entity.types.DidacticActivityStatus;
import be.entity.types.WeekType;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

public class TeacherDidacticDTO implements Serializable {
    private Integer id;
    private String courseName;
    private String activityType;
    private Date date;
    private Time time;
    private Integer duration;
    private String room;
    private String specialization;
    private Integer group;
    private Integer semigroup;
    private WeekType weekType;
    private DayOfTheWeek dayOfTheWeek;
    private DidacticActivityColor color;
    private String description;

    public TeacherDidacticDTO(Integer id, String courseName, String activityType, Date date, Time time, Integer duration, String room, String specialization, Integer group, Integer semigroup,
                              WeekType weekType, DayOfTheWeek dayOfTheWeek, DidacticActivityColor color, String description) {
        this.id = id;
        this.courseName = courseName;
        this.activityType = activityType;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.room = room;
        this.specialization = specialization;
        this.group = group;
        this.semigroup = semigroup;
        this.weekType = weekType;
        this.dayOfTheWeek = dayOfTheWeek;
        this.color = color;
        this.description = description;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getSemigroup() {
        return semigroup;
    }

    public void setSemigroup(Integer semigroup) {
        this.semigroup = semigroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public WeekType getWeekType() {
        return weekType;
    }

    public void setWeekType(WeekType weekType) {
        this.weekType = weekType;
    }

    public DayOfTheWeek getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    public void setDayOfTheWeek(DayOfTheWeek dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }

    public DidacticActivityColor getColor() {
        return color;
    }

    public void setColor(DidacticActivityColor color) {
        this.color = color;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
