package be.dto;

import be.entity.Request;

import be.entity.types.RequestStatus;
import be.entity.types.RequestType;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;


/**
 * The class maps a {@link Request} object.
 *
 * @author Mara Corina
 */
public class RequestDTO implements Serializable {
    private Integer ID;
    private RequestType type;
    private String description;
    private Date date;
    private Time time;
    private RequestStatus status;
    private UserDTO requester;
    private DidacticActivityDTO activity;
    private Integer currentActivityId;

    public RequestDTO() {
    }

    public RequestDTO(Integer ID, RequestType type, String description, Date date, Time time, RequestStatus status, UserDTO requester,
                      DidacticActivityDTO activity, Integer currentActivityId) {
        this.ID = ID;
        this.type = type;
        this.description = description;
        this.date = date;
        this.time = time;
        this.status = status;
        this.requester = requester;
        this.activity = activity;
        this.currentActivityId = currentActivityId;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public UserDTO getRequester() {
        return requester;
    }

    public void setRequester(UserDTO requester) {
        this.requester = requester;
    }

    public DidacticActivityDTO getActivity() {
        return activity;
    }

    public void setActivity(DidacticActivityDTO activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
                "ID=" + ID +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", time=" + time +
                ", status=" + status +
                ", requester=" + requester +
                ", activity=" + activity +
                '}';
    }


    public Integer getCurrentActivityId() {
        return currentActivityId;
    }

    public void setCurrentActivityId(Integer currentActivityId) {
        this.currentActivityId = currentActivityId;
    }

}
