package be.dto;

import be.entity.Role;
import be.entity.User;
import java.util.*;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * The class maps a {@link User} object.
 *
 * @author Mara Corina
 */
public class UserDTO implements Serializable {

    //keep token string
    private String token;

    private Integer ID;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private Integer counter;
    private String email;
    private String mobileNumber;
    private Integer status;
    private Integer grupa;
    private Integer semigrupa;
    private Set<Role> roles;

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Integer getGrupa() {
        return grupa;
    }

    public void setGrupa(Integer grupa) {
        this.grupa = grupa;
    }

    public Integer getSemigrupa() {
        return semigrupa;
    }

    public void setSemigrupa(Integer semigrupa) {
        this.semigrupa = semigrupa;
    }

    public UserDTO() {
    }

    public UserDTO(Integer counter, String firstName, String lastName, String mobileNumber, String email,
                   String username, Integer status, Integer grupa, Integer semigrupa) {

        this.counter = counter;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.username = username;
        this.password = "";
        this.status = status;
        this.grupa = grupa;
        this.semigrupa = semigrupa;
        this.roles = new HashSet<>();

    }

    public UserDTO(Integer counter, String firstName, String lastName, String mobileNumber, String email,
                   String username, String password, Integer status) {
        this.counter = counter;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer id) {
        this.ID = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + ID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", counter=" + counter +
                ", email='" + email + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", status=" + status +
                '}';
    }
}
