package be.manager.remote;

import be.dto.ChangePasswordDTO;
import be.dto.NotificationDTO;
import be.dto.UserDTO;
import be.entity.types.LoginType;
import be.exceptions.BusinessException;

import java.util.List;
import java.util.Set;

/**
 * Interface for Remote usage
 *
 * @author Mara Corina
 */
public interface UserManagerRemote {

    UserDTO insertUser(UserDTO userDTO) throws BusinessException;

    UserDTO findUser(Integer id) throws BusinessException;

    Set<NotificationDTO> getUserNotifications(String username) throws BusinessException;

    List<UserDTO> findAllUsers();

    UserDTO findUserByUsernameAndPassword(String username, String password) throws BusinessException;

    List<Integer> getAllGroups();

    UserDTO changePassword(ChangePasswordDTO changePasswordDTO) throws BusinessException;
}
