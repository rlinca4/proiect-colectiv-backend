package be.manager.remote;

import be.dto.DidacticActivityDTO;
import be.dto.StudentActivityMapperDTO;
import be.exceptions.BusinessException;

import java.util.List;

public interface StudentDidacticRemote {

    List<DidacticActivityDTO> getAllStudentDidacticActivities(int studentId);

    StudentActivityMapperDTO insertUser(Integer userId, Integer activityId) throws BusinessException, Exception;
    StudentActivityMapperDTO deleteActivitiesForUser(Integer userId, Integer activityId) throws Exception;
}
