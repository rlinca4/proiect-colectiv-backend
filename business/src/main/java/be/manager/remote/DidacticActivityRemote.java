package be.manager.remote;

import be.dto.DidacticActivityDTO;
import be.entity.DidacticActivity;
import be.exceptions.BusinessException;

import java.util.List;

public interface DidacticActivityRemote {

    DidacticActivity getByCourseNameAndSpecialization(String courseName, String specialization) throws BusinessException;
    List<DidacticActivityDTO> getAllActivitiesForChange(DidacticActivityDTO didacticActivityDTO);
    DidacticActivity update(DidacticActivityDTO didacticActivityDTO);
    List<DidacticActivity> getAllCourses();

}
