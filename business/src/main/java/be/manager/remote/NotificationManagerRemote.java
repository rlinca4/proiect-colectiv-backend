package be.manager.remote;

import be.dto.NotificationDTO;
import be.dto.UserDTO;

/**
 * Interface for Remote usage
 *
 * @author Mara Corina
 */
public interface NotificationManagerRemote {

    NotificationDTO insertNotification(NotificationDTO notification);

    void insertWelcomeNotification(UserDTO userDTO);

    void insertDeletedUserNotification(UserDTO userDTO, UserDTO receiverDTO);

    void insertDeactivatedUserNotification(UserDTO userDTO, UserDTO receiverDTO);

    void insertUserUpdatedNotification(UserDTO newUserDTO, UserDTO oldUserDTO, UserDTO updaterDTO);

    void clean();
}
