package be.manager.remote;

import be.dto.TeacherDidacticDTO;
import java.util.List;
public interface TeacherDidacticRemote {

    List<TeacherDidacticDTO> getAllTeacherDidacticActivities(int tutorId);
}
