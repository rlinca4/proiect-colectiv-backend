package be.manager.remote;

import be.dto.RequestDTO;
import be.exceptions.BusinessException;
import be.exceptions.ValidationException;

import java.util.List;

/**
 * Interface for Remote usage
 *
 * @author Mara Corina
 */
public interface RequestManagerRemote {

    RequestDTO createRequest(RequestDTO requestDTO) throws BusinessException, ValidationException;

    List<RequestDTO> getAllForUser(Integer id) throws BusinessException;

}
