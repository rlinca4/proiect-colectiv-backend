package be.manager.remote;

import be.dto.EmailDTO;
import be.exceptions.BusinessException;

public interface EmailManagerRemote {

    void sendSimpleMessage(EmailDTO emailDTO) throws BusinessException;
}
