package be.manager.validator;

import be.dto.RequestDTO;
import be.entity.Request;

/**
 * The class validator for {@link Request} objects.
 *
 * @author Mara Corina
 */
public class RequestValidator {
    public static boolean isValidForAdd(RequestDTO requestDTO) {
        if(requestDTO.getType() == null)
            return false;
        if(requestDTO.getRequester() == null)
            return false;
        if(requestDTO.getRequester().getID() == null)
            return false;
        if(requestDTO.getActivity() == null)
            return false;
        if(requestDTO.getActivity().getID() == null)
            return false;

        return true;
    }
}
