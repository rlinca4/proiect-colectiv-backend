package be.manager.validator;

import be.dto.UserDTO;
import be.entity.User;

/**
 * The class validator for {@link User} objects.
 *
 * @author Mara Corina
 */
public class UserValidator {
    public static boolean isValidForAdd(UserDTO userDTO) {
        if(!userDTO.getFirstName().matches("[a-zA-Z]{1,50}]"))//numele prenume sa aibe doar caractere intre a-z A-Z si maxim 50 de caractere
            return false;
        if(!userDTO.getLastName().matches("[a-zA-Z]{1,50}]"))
            return false;
        if(!userDTO.getMobileNumber().matches("[0-9]{10}]"))//nrtel doar 10 caractere si toate sa fie doar cifre
            return false;
        if(!userDTO.getUsername().matches("[a-zA-Z0-9]{1,50}"))//username/parola sa aibe a-z A-Z 0-9  poate si &#$
            return false;
        if(!userDTO.getPassword().matches("[a-zA-Z0-9]{1,50}"))
            return false;
        if(userDTO.getEmail().matches("[-a-zA-Z_]{1,50}@[a-zA-Z]{1,50}.[a-zA-Z]{1,5}"))//email text @ text . text
            return false;
//        if(userDTO.getCounter())    ??
//            return false;
//        if(userDTO.getStatus())    ??
//            return false;
        return true;
    }
}
