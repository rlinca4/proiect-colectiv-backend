package be.manager.validator;

import be.dto.EmailDTO;

public class EmailValidator {
    public static boolean isValid(EmailDTO emailDTO) {
        if(emailDTO.getTo() == null) return false;
        if(emailDTO.getSubject() == null) return false;
        if(emailDTO.getText() == null) return false;

        return true;
    }
}
