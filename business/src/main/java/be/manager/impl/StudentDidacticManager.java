package be.manager.impl;

import be.dao.DidacticActivityDao;
import be.dao.StudentActivityMapperDao;
import be.dao.UserDao;
import be.dto.DidacticActivityDTO;
import be.dto.StudentActivityMapperDTO;
import be.dtoEntityMappers.DidacticActivityDTOEntityMapper;
import be.dtoEntityMappers.StudentActivityMapperDTOEntityMapper;
import be.entity.DidacticActivity;
import be.entity.StudentActivityMapper;
import be.entity.User;
import be.exceptions.BusinessException;
import be.manager.remote.StudentDidacticRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class StudentDidacticManager implements StudentDidacticRemote {

    @Autowired
    StudentActivityMapperDao studentActivityMapperDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    DidacticActivityDao didacticActivityDao;

    @Override
    public List<DidacticActivityDTO> getAllStudentDidacticActivities(int studentId) {
        return studentActivityMapperDao.findAllByStudentId(studentId)
                .stream()
                .map(DidacticActivityDTOEntityMapper::getDidacticActivityDTOFromEntity)
                .collect(Collectors.toList());
    }

    /**
     * Inserts a new {@link StudentActivityMapper} in the database and
     * corresponding activities, like lab or seminary for that course activity
     * @param userId is the id for an {@link User} persisted in the database.
     * @param activityId is the id for an {@link DidacticActivity} persisted in the database.
     * @return {@StudentActivityMapperDTO} object mapping the entity persisted in the database
     * @throws {@link BusinessException} if the corresponding {@link User} object or
     *          {@link DidacticActivity} object is not present in the database
     * @author Mara Corina
     */
    @Override
    public StudentActivityMapperDTO insertUser(Integer userId, Integer activityId) throws Exception {
        Optional<User> user= userDao.findById(userId);
        if (!user.isPresent()) {
            throw new BusinessException("Find user error: ", "No user found!");
        }
        Optional<DidacticActivity> didacticActivity = didacticActivityDao.findByID(activityId);
        if (!didacticActivity.isPresent()) {
            throw new BusinessException("Find didactic activity error: ", "No activity found!");
        }
        StudentActivityMapper persistedStudentActivityEntity;

        List<StudentActivityMapper> existedActivityMapper = studentActivityMapperDao.findByStudentIDAndActivityID(userId, activityId);
        if(existedActivityMapper.size() > 0) {
            throw new Exception("Activity with " + activityId + "already exist for student " + userId);
        }
        else {
            // Add course
            StudentActivityMapper studentActivityEntity = new StudentActivityMapper();
            studentActivityEntity.setStudent(user.get());
            studentActivityEntity.setActivity(didacticActivity.get());
            persistedStudentActivityEntity = studentActivityMapperDao.save(studentActivityEntity);

            List<DidacticActivity> othersActivities = didacticActivityDao.getCorrespondingActivitiesByCourse
                    (didacticActivity.get().getCourseName(), user.get().getGrupa(), user.get().getSemigrupa());
            if(othersActivities.size() > 0) {
                for (DidacticActivity da: othersActivities) {
                    existedActivityMapper = studentActivityMapperDao.findByStudentIDAndActivityID(user.get().getID(), da.getID());
                    if(existedActivityMapper.size() == 0) {
                        StudentActivityMapper sAOther = new StudentActivityMapper();
                        sAOther.setStudent(user.get());
                        sAOther.setActivity(da);
                        persistedStudentActivityEntity = studentActivityMapperDao.save(sAOther);
                    }

                }
            }
        }

        return StudentActivityMapperDTOEntityMapper.getDTOFromStudentActivityMapper(persistedStudentActivityEntity);
    }

    @Override
    public StudentActivityMapperDTO deleteActivitiesForUser(Integer userId, Integer activityId) throws Exception {
        Optional<User> user= userDao.findById(userId);
        if (!user.isPresent()) {
            throw new BusinessException("Find user error: ", "No user found!");
        }
        Optional<DidacticActivity> didacticActivity = didacticActivityDao.findByID(activityId);
        if (!didacticActivity.isPresent()) {
            throw new BusinessException("Find didactic activity error: ", "No activity found!");
        }

        List<StudentActivityMapper> existedActivityMapper = studentActivityMapperDao.findByStudentIDAndActivityID(userId, activityId);
        if(existedActivityMapper.size() == 0) {
            throw new Exception("Activity with " + activityId + "does not exist for student " + userId);
        }
        else {
            studentActivityMapperDao.deleteByStudentAndActivity(user.get(), didacticActivity.get());
            List<DidacticActivity> myActivities = studentActivityMapperDao.findAllByStudentId(user.get().getID());
            for (DidacticActivity da: myActivities) {
                if(da.getCourseName().equals(didacticActivity.get().getCourseName()))
                {
                    studentActivityMapperDao.deleteByStudentAndActivity(user.get(), da);
                }
            }
        }

        return StudentActivityMapperDTOEntityMapper.getDTOFromStudentActivityMapper(existedActivityMapper.get(0));
    }
}
