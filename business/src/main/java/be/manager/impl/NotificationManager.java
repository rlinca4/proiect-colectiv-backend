package be.manager.impl;

import be.dao.NotificationDao;
import be.dto.NotificationDTO;
import be.dto.UserDTO;
import be.dtoEntityMappers.NotificationDTOEntityMapper;
import be.dtoEntityMappers.UserDTOEntityMapper;
import be.entity.Notification;
import be.entity.User;
import be.entity.types.NotificationType;
import be.manager.remote.NotificationManagerRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;

@Component
public class NotificationManager implements NotificationManagerRemote {

    @Autowired
    private NotificationDao notificationDao;

    public NotificationDao getNotificationDao() {
        return notificationDao;
    }

    public void setNotificationDao(NotificationDao notificationDao) {
        this.notificationDao = notificationDao;
    }

    public NotificationDTO insertNotification(NotificationDTO notification){

        notificationDao.save(NotificationDTOEntityMapper.getNotificationFromDTO(notification));
        return notification;
    }

    public Notification createNewNotification(NotificationType type, String message, String url, User user) {
        Notification notification = new Notification(Date.valueOf(LocalDate.now()), message, type, url, user);

        return notification;
    }

    @Override
    public void insertWelcomeNotification(UserDTO userDTO) {
        String message = "Welcome, " + userDTO.getFirstName() + " " + userDTO.getLastName() + "!" + '\n' +
                '\n' +
                "Email : " + userDTO.getEmail() + '\n' +
                "First Name : " + userDTO.getFirstName() + '\n' +
                "Last Name : " + userDTO.getLastName() + '\n' +
                "Mobile Number : " + userDTO.getMobileNumber() + '\n' +
                "Username : " + userDTO.getUsername() + '\n';
        User user = UserDTOEntityMapper.getUserFromUserDTO(userDTO);
        Notification notification = createNewNotification(NotificationType.WELCOME_NEW_USER, message, "", user);

        notificationDao.save(notification);
    }

    @Override
    public void insertDeletedUserNotification(UserDTO userDTO, UserDTO receiverDTO) {
        String message = "User " + userDTO.getUsername() + " was deleted!" + '\n' +
                '\n' +
                "Email : " + userDTO.getEmail() + '\n' +
                "First Name : " + userDTO.getFirstName() + '\n' +
                "Last Name : " + userDTO.getLastName() + '\n' +
                "Mobile Number : " + userDTO.getMobileNumber() + '\n' +
                "Username : " + userDTO.getUsername() + '\n';
        User receiver = UserDTOEntityMapper.getUserFromUserDTO(receiverDTO);
        Notification notification = createNewNotification(NotificationType.USER_DELETED, message, "", receiver);

        notificationDao.save(notification);
    }

    @Override
    public void insertDeactivatedUserNotification(UserDTO userDTO, UserDTO admin) {

        String message = "User " + userDTO.getUsername() + " was deactivated!" + '\n' +
                '\n' +
                "Email : " + userDTO.getEmail() + '\n' +
                "First Name : " + userDTO.getFirstName() + '\n' +
                "Last Name : " + userDTO.getLastName() + '\n' +
                "Mobile Number : " + userDTO.getMobileNumber() + '\n' +
                "Username : " + userDTO.getUsername() + '\n';
        User receiver = UserDTOEntityMapper.getUserFromUserDTO(admin);
        Notification notification = createNewNotification(NotificationType.USER_DEACTIVATED, message, "", receiver);

        notificationDao.save(notification);
    }

    @Override
    public void insertUserUpdatedNotification(UserDTO newUserDTO, UserDTO oldUserDTO, UserDTO updaterDTO) {
        String message = "User account updated!" + '\n' +
                "NEW INFO " + '\n' +
                "\tEmail : " + newUserDTO.getEmail() + '\n' +
                "\tFirst Name : " + newUserDTO.getFirstName() + '\n' +
                "\tLast Name : " + newUserDTO.getLastName() + '\n' +
                "\tMobile Number : " + newUserDTO.getMobileNumber() + '\n' +
                "\tUsername : " + newUserDTO.getUsername() + '\n' +
                '\n' +
                "OLD INFO " + '\n' +
                "\tEmail : " + oldUserDTO.getEmail() + '\n' +
                "\tFirst Name : " + oldUserDTO.getFirstName() + '\n' +
                "\tLast Name : " + oldUserDTO.getLastName() + '\n' +
                "\tMobile Number : " + oldUserDTO.getMobileNumber() + '\n' +
                "\tUsername : " + oldUserDTO.getUsername() + '\n';

        User user = UserDTOEntityMapper.getUserFromUserDTO(newUserDTO);
        Notification notificationUser = createNewNotification(NotificationType.USER_UPDATED, message, "", user);

        User updater = UserDTOEntityMapper.getUserFromUserDTO(updaterDTO);
        Notification notificationUpdater = createNewNotification(NotificationType.USER_UPDATED, message, "", updater);

        notificationDao.save(notificationUser);
        notificationDao.save(notificationUpdater);
    }

    @Override
    public void clean() {




    }

//    @Override
//    public void clean() {
//        notificationDao.deleteOlderThanOneMonth();
//    }

}
