package be.manager.impl.email;

import be.dto.EmailDTO;
import be.exceptions.BusinessException;
import be.manager.remote.EmailManagerRemote;
import be.manager.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailManager implements EmailManagerRemote {

    @Autowired
    public JavaMailSender emailSender;

    public void sendSimpleMessage(
            EmailDTO emailDTO) throws BusinessException {

        if(EmailValidator.isValid(emailDTO)) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(emailDTO.getTo());
            message.setSubject(emailDTO.getSubject());
            message.setText(emailDTO.getText());
            emailSender.send(message);
        }
        else
            throw new BusinessException("Email error",
                    "Please complete with corresponding values");
    }
}