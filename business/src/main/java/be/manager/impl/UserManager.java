package be.manager.impl;

import be.dao.RoleDao;
import be.dao.UserDao;
import be.dao.UserRoleDao;
import be.dto.ChangePasswordDTO;
import be.dto.NotificationDTO;
import be.dto.UserDTO;
import be.dtoEntityMappers.UserDTOEntityMapper;
import be.entity.Notification;
import be.entity.Role;
import be.entity.User;
import be.entity.UsersRoles;
import be.entity.types.RoleType;
import be.exceptions.BusinessException;
import be.manager.remote.NotificationManagerRemote;
import be.manager.remote.UserManagerRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.logging.Logger;

/**
 * Manager class for CRUD actions on {@link User} objects.
 */
@Component
public class UserManager implements UserManagerRemote {

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    public UserDao getUserDao() {
        return userDao;
    }

    @Autowired
    private NotificationManagerRemote notificationManager;

    public NotificationManagerRemote getNotificationManager() {
        return notificationManager;
    }

    public void setNotificationManager(NotificationManagerRemote notificationManager) {
        this.notificationManager = notificationManager;
    }

    private Logger logger = Logger.getLogger(UserManager.class.getName());

    /**
     * @param userDTO is an {@link UserDTO} object that maps the {@link User}
     *                object that will be persisted in the database.
     *                Inserts a WELCOME_NEW_USER {@link Notification} object in the database
     * @throws {@link BusinessException} if the {@link UserDTO} object is
     *                invalid
     * @author Mara Corina
     */
    @Override
    public UserDTO insertUser(UserDTO userDTO){
        User user = createUserToInsert(userDTO);
        User persistedUser;
        if(user.getRoles().contains(roleDao.findByType(RoleType.TEACHER))){
            User actualUser = this.userDao.findByUsernameAndStatus(user.getUsername(), user.getStatus());
            actualUser.setMobileNumber(user.getMobileNumber());
            actualUser.setEmail(user.getEmail());
            actualUser.setPassword(user.getPassword());
            actualUser.setStatus(user.getStatus());
            actualUser.setRoles(user.getRoles());
            int persisted = userDao.update(actualUser.getMobileNumber(), actualUser.getEmail(), actualUser.getPassword(), actualUser.getStatus(), actualUser.getID());
            persistedUser = actualUser;
            this.userRoleDao.save(new UsersRoles(actualUser, roleDao.findByType(RoleType.TEACHER)));
        }
        else {
            persistedUser = userDao.save(user);
            this.userRoleDao.save(new UsersRoles(persistedUser, roleDao.findByType(RoleType.STUDENT)));
        }

        UserDTO dtoPersisted = UserDTOEntityMapper.getDTOCompleteFromUser(persistedUser);
        return dtoPersisted;
    }

    /**
     * Transforma din UserDTO in User
     * @param userDTO
     * @return
     */
    private User createUserToInsert(UserDTO userDTO) {
        User user = new User(userDTO.getCounter(), userDTO.getFirstName(), userDTO.getLastName(),userDTO.getMobileNumber(),userDTO.getEmail(),userDTO.getUsername(), userDTO.getPassword(), userDTO.getStatus(), userDTO.getGrupa(), userDTO.getSemigrupa());

        if(this.userDao.findByUsernameAndStatus(user.getUsername(), user.getStatus()) == null)
            user.getRoles().add(this.roleDao.findByType(RoleType.STUDENT));
        else
            user.getRoles().add(this.roleDao.findByType(RoleType.TEACHER));

        return user;
    }

    /**
     * Generates a unique username for the ready to insert {@link User} object
     *      using the user firstname and lastname
     * @param firstName and lastName
     *
     * @return {@link String} representing the username
     */
    public String generateUsername(String firstName, String lastName) {

        //uses only the first 5 characters from the lastName
        String firstPart;
        if(lastName.length() >= 5){

            firstPart = lastName.substring(0, 5);
        } else {

            firstPart = lastName;
        }

        //adds characters from the firstName to the username until the username is unique
        //when it reaches the end of the firstName it adds 'x' characters
        String username = "";
        int charPosition = 0;
        if(firstName != null) {
            username = (firstPart + firstName).toLowerCase();
        }
        else {
            username = firstPart;
        }
        while(userDao.findUserByUsername(username) != null){
            charPosition++;
            if(charPosition < firstName.length()){
                username = (username + firstName.charAt(charPosition)).toLowerCase();
            } else{
                username = username + "x";
            }
        }
        return username;
    }

    /**
     * Generates a password for the ready to insert {@link User} object
     *
     * @return {@link String} representing the password
     * @author Mara Corina
     */
    public String generatePassword(){
        String password = new Random().ints(10, 33, 122).collect(StringBuilder::new,
                StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return password;
    }

    @Override
    public UserDTO findUser(Integer id) throws BusinessException {
        User user = userDao.findAllByID(id);
        if (user == null) {
            throw new BusinessException("Find user error: ", "No user with this id was found!");
        }
        return UserDTOEntityMapper.getDTOCompleteFromUser(user);
    }

//    /**
//     * Returns a set of {@link NotificationDTO} objects that wrap the {@link Notification} objects
//     * corresponding to the user with the username given as parameter from the database
//     *
//     * @param username {@link String}
//     * @throws {@link BusinessException} if there is no user with the given username in the database
//     */
//    @Override
//    public Set<NotificationDTO> getUserNotifications(String username) {
//        return NotificationDTOEntityMapper.getNotificationDTOListFromNotificationList(new HashSet<>(userDao.getNotificationsByUsername(username)));
//    }

    @Override
    public Set<NotificationDTO> getUserNotifications(String username) throws BusinessException {
        return null;
    }

    @Override
    public List<UserDTO> findAllUsers(){
        List<User> users = userDao.findAll();
        return UserDTOEntityMapper.getUserDTOListWithRolesWithoutPasswFromUserList(users);
    }

    @Override
    public UserDTO findUserByUsernameAndPassword(String username, String password) throws BusinessException {
        User user= userDao.findByUsernameAndPassword(username,password);
        if (user == null) {
            throw new BusinessException("Find user error: ", "No user with this username and password was found!");
        }

        List<UsersRoles> usersRoles = this.userRoleDao.findByUserId(user.getID());
        Set<Role> roles = new HashSet<>();
        for (UsersRoles ur: usersRoles)
        {
            roles.add(ur.getRole());
        }
        user.setRoles(roles);
        return UserDTOEntityMapper.getDTOCompleteFromUser(user);
    }

//    /**
//     * @param userUpdateDTO is an {@link UserUpdateDTO} object that wraps a
//     *                {@link UserDTO} object that contains the updated info
//     *                     of the {@link User} object that will be updated in the database and a
//     *                {@link String} object corresponding to the user that realized the update,
//     *
//     * @return an {@link UserDTO} object with the persisted informations
//     * @throws {@link BusinessException} if the {@link UserDTO} object is
//     *                invalid or doesn't have a corresponding object in the database
//     * @author Mara Corina
//     */
//    @Override
//    public UserDTO updateUser(UserUpdateDTO userUpdateDTO) throws BusinessException {
//        UserDTO userDTO = userUpdateDTO.getUser();
//        UserValidator.validateForUpdate(userDTO);
//        User persistedUser = userDao.findUser(userDTO.getId());
//        UserDTO oldUserValue = UserDTOEntityMapper.getDTOFromUser(persistedUser);
//
//        if (persistedUser == null)
//            throw new BusinessException("msg8_10_1101", "No user was found!");
//
//        //persist the updated info
//        persistedUser.setCounter(userDTO.getCounter());
//        persistedUser.setEmail(userDTO.getEmail());
//        persistedUser.setFirstName(userDTO.getFirstName());
//        persistedUser.setLastName(userDTO.getLastName());
//        persistedUser.setMobileNumber(userDTO.getMobileNumber());
//
//        //persist new password
//        //if password was not updated, keep the old one
//        if (!userDTO.getPassword().equals("")) {
//            //hash password
//            String hashPassword = sha256()
//                    .hashString(userDTO.getPassword(), StandardCharsets.UTF_8)
//                    .toString();
//            persistedUser.setPassword(hashPassword);
//        }
//
//        //take the coresponding roles from the database and persist them
//        persistedUser.setRoles(getActualRoleList(userDTO.getRoles()));
//
//        //set the counter to 0 if the user was activated
//        if (persistedUser.getStatus() == 0 && userDTO.getStatus() == 1)
//            persistedUser.setCounter(0);
//
//        //do not allow deactivation if user has assigned bugs
//        if (persistedUser.getStatus() == 1 && userDTO.getStatus() == 0 && persistedUser.getAssignedBugs().size() > 0)
//            persistedUser.setStatus(1);
//        else
//            persistedUser.setStatus(userDTO.getStatus());
//
//        UserDTO newUserValue = UserDTOEntityMapper.getDTOFromUser(persistedUser);
//
//        //send the specific notifications
//        sendUpdatedOrDeletedUserNotification(oldUserValue, newUserValue, userUpdateDTO.getUsernameUpdater());
//
//        return newUserValue;
//    }

    /**
     * Returns a list of all the distinct not null didactic groups in the database
     * @return a list of Integers
     * @author Mara Corina
     */
    @Override
    public List<Integer> getAllGroups(){
        List<Integer> allGroups = this.userDao.getAllGroups();
        allGroups.removeAll(Collections.singleton(null));
        return allGroups;
    }

    @Override
    public UserDTO changePassword(ChangePasswordDTO changePasswordDTO) throws BusinessException {
        User user = this.userDao.findAllByID(changePasswordDTO.getUserId());
        if(user.getPassword().equals(changePasswordDTO.getOldPassword())) {
            // Change password;
            user.setPassword(changePasswordDTO.getNewPassword());
            int updated = this.userDao.updatePassword(changePasswordDTO.getUserId(), changePasswordDTO.getNewPassword());
            return UserDTOEntityMapper.getDTOCompleteFromUser(user);
        }
        else
            throw new BusinessException("Password error", "The old password is wrong");
    }

}
