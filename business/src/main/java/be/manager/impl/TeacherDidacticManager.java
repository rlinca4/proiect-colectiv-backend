package be.manager.impl;

import be.dao.DidacticActivityDao;
import be.dto.TeacherDidacticDTO;
import be.entity.DidacticActivity;
import be.manager.remote.TeacherDidacticRemote;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class TeacherDidacticManager implements TeacherDidacticRemote {
    @Autowired
    DidacticActivityDao didacticActivityDao;

    @Override
    public List<TeacherDidacticDTO> getAllTeacherDidacticActivities(int tutorId) {
        List<DidacticActivity> didacticActivities = didacticActivityDao.getDidacticActivitiesByTutorID(tutorId);
        List<TeacherDidacticDTO> teacherDidacticDTOS = new ArrayList<>();
        // Add logic
        for (DidacticActivity da: didacticActivities) {
            teacherDidacticDTOS.add(new TeacherDidacticDTO(
                    da.getID(),
                    da.getCourseName(),
                    da.getType().getActualString(),
                    da.getDate(),
                    da.getTime(),
                    da.getDuration(),
                    da.getRoom(),
                    da.getSpecialization(),
                    da.getGrupa(),
                    da.getSemigrupa(),
                    da.getWeekType(),
                    da.getDayOfTheWeek(),
                    da.getColor(),
                    da.getDescription()
            ));
        }

        return teacherDidacticDTOS;
    }
}
