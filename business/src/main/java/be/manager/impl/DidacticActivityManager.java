package be.manager.impl;

import be.dao.DidacticActivityDao;
import be.dao.ExchangeStudentsDao;
import be.dao.NotificationDao;
import be.dao.UserDao;
import be.dto.DidacticActivityDTO;
import be.dtoEntityMappers.DidacticActivityDTOEntityMapper;
import be.entity.DidacticActivity;
import be.entity.ExchangeStudent;
import be.entity.Notification;
import be.entity.User;
import be.entity.types.DidacticActivityColor;
import be.entity.types.NotificationType;
import be.exceptions.BusinessException;
import be.manager.remote.DidacticActivityRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DidacticActivityManager implements DidacticActivityRemote {
    @Autowired
    DidacticActivityDao didacticActivityDao;
    @Autowired
    ExchangeStudentsDao exchangeStudentsDao;
    @Autowired
    UserDao userDao;
    @Autowired
    NotificationDao notificationDao;

    @Override
    public DidacticActivity getByCourseNameAndSpecialization(String courseName,String specialization) throws BusinessException {
        DidacticActivity didacticActivity = didacticActivityDao
                .findByCourseNameAndSpecialization(courseName, specialization);
        if(didacticActivity == null)
            throw new BusinessException("Find didacticActivity error: ",
                    "No didacticActivity with this course and this specialization was found!");
        return didacticActivity;
    }

    public void changeClass(DidacticActivity newActivity, User user) throws BusinessException {
        DidacticActivity old = new DidacticActivity();
        old.setCourseName(newActivity.getCourseName());    //numele cursului
        old.setType(newActivity.getType());                //specializare

        old=findDidacticActivity(old, user.getDidacticActivities());

        notificationDao.save(new Notification(new Date(System.currentTimeMillis()),
                "Hi, my name is " + user.getLastName()+" "
                        + user.getFirstName()+". I am gonna be your new student.",
                NotificationType.USER_UPDATED,
                "",
                newActivity.getTutor()
        ));
        user.getDidacticActivities().remove(old);
        user.getDidacticActivities().add(newActivity);
    }
    public boolean changeClassConditioned(DidacticActivity newActivity, User user) throws BusinessException {
        DidacticActivity old = new DidacticActivity();
        old.setCourseName(newActivity.getCourseName());    //numele cursului
        old.setType(newActivity.getType());                //specializare

        old=findDidacticActivity(old, user.getDidacticActivities());

        ExchangeStudent otherStudent=exchangeStudentsDao.findByIDDidacticActivity(old.getID());
        if(exchangeStudentsDao!=null) {
            changeClass(newActivity,user);
            changeClass(old,userDao.findAllByID(otherStudent.getIDUser()));
            exchangeStudentsDao.delete(otherStudent);
            return true; //Accepted
        }else{
            exchangeStudentsDao.saveAndFlush(otherStudent);     //what could go wrong?
        }
        return false;
    }
    private DidacticActivity findDidacticActivity(DidacticActivity didacticActivity, Set<DidacticActivity> activities) throws BusinessException {
        for (DidacticActivity activity:activities) {
            if(activity.equals(didacticActivity)){
                return activity;
            }
        }
        throw new BusinessException("Find didacticActivity error: ", "The didacticActivity does not exists in the set (activities)");
    }

    @Override
    public List<DidacticActivityDTO> getAllActivitiesForChange(DidacticActivityDTO didacticActivityDTO) {
        return didacticActivityDao.findActivitiesForChange(didacticActivityDTO.getID(),
                didacticActivityDTO.getCourseName(), didacticActivityDTO.getType())
                .stream()
                .map(DidacticActivityDTOEntityMapper::getDidacticActivityDTOFromEntity)
                .collect(Collectors.toList());
    }

    @Override
    /**
     * Update -> status
     *           color
     */
    public DidacticActivity update(DidacticActivityDTO didacticActivityDTO) {
        if(didacticActivityDTO.getStatus() == null || didacticActivityDTO.getColor() == null) {
            return null;
        }
        else {
            this.didacticActivityDao.update(didacticActivityDTO.getID(), didacticActivityDTO.getStatus(), didacticActivityDTO.getColor());
            Optional<DidacticActivity> updatedDidacticActivity = didacticActivityDao.findByID(didacticActivityDTO.getID());
            return updatedDidacticActivity.orElse(null);
        }
    }

    @Override
    public List<DidacticActivity> getAllCourses() {
//        return this.didacticActivityDao.getAllCourses();
////        return this.didacticActivityDao.getAllCourses().stream()
////                .map(DidacticActivity::getCourseName)
////                .collect(Collectors.toList());

        List<DidacticActivity> didacticActivities = this.didacticActivityDao.getAllCoursesWithId();
        for (DidacticActivity da: didacticActivities) {
            da.setTutor(null);
        }
        return didacticActivities;
    }
}
