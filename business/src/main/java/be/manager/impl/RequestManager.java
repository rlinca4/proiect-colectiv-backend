package be.manager.impl;

import be.dao.DidacticActivityDao;
import be.dao.RequestDao;
import be.dao.StudentActivityMapperDao;
import be.dao.UserDao;
import be.dto.EmailDTO;
import be.dto.RequestDTO;
import be.dtoEntityMappers.RequestDTOEntityMapper;
import be.entity.DidacticActivity;
import be.entity.Request;
import be.entity.StudentActivityMapper;
import be.entity.User;
import be.entity.types.DidacticActivityColor;
import be.entity.types.RequestStatus;
import be.entity.types.RequestType;
import be.exceptions.BusinessException;
import be.exceptions.ValidationException;
import be.manager.impl.email.EmailManager;
import be.manager.remote.DidacticActivityRemote;
import be.manager.remote.NotificationManagerRemote;
import be.manager.remote.RequestManagerRemote;
import be.manager.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Manager class for CRUD actions on {@link Request} objects.
 *
 * @author Mara Corina
 */
@Component
public class RequestManager implements RequestManagerRemote {

    @Autowired
    private RequestDao requestDao;

    @Autowired
    private NotificationManagerRemote notificationManager;

    @Autowired
    private StudentActivityMapperDao studentActivityMapperDao;

    @Autowired
    private DidacticActivityDao didacticActivityDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private EmailManager emailManager;

    public NotificationManagerRemote getNotificationManager() {
        return notificationManager;
    }

    public void setNotificationManager(NotificationManagerRemote notificationManager) {
        this.notificationManager = notificationManager;
    }

    private Logger logger = Logger.getLogger(UserManager.class.getName());

    /**
     * Creates a {@link Request} object of the ready to insert {@link RequestDTO} object
     *      using the type
     * @param requestDTO is an {@link RequestDTO} object that maps the {@link Request}
     *      *                object that will be persisted in the database.
     *      *                If the type is REQUEST, saves a new request
     *                       If the type is ACCEPT, accepts an existing request (updates status)
     *                       If the type is DENY, denies the request (updates status)
     * @throws {@link BusinessException} if the object already exists in the first case
     * @throws {@link BusinessException} if the object doesn't exist in the other cases
     * @return {@link RequestDTO} representing the mapper of the persisted {@link Request} object
     * @author Mara Corina
     */
    @Override
    public RequestDTO createRequest(RequestDTO requestDTO) throws BusinessException, ValidationException {
        if(!RequestValidator.isValidForAdd(requestDTO))
            throw new ValidationException("RequestError", "Invalid request attributes!");
            Request request = RequestDTOEntityMapper.getRequestFromDTOEntity(requestDTO);
            if(request.getType() == RequestType.REQUEST) {
                DidacticActivity reqDidacticActivity = didacticActivityDao.findByID(request.getActivity().getID()).get();
                if(reqDidacticActivity.getColor().equals(DidacticActivityColor.GREEN)) {
                    //Update student activity
                    DidacticActivity similarDidacticActivity = didacticActivityDao.findByID(request.getCurrentIdActivity()).get();

                    studentActivityMapperDao.updateActivity(request.getRequester().getID(), request.getActivity().getID(), similarDidacticActivity.getID());
                    request.setStatus(RequestStatus.ACCEPTED);
                    request.setDate(new Date(Calendar.getInstance().getTime().getTime()));
                    request.setTime(new Time(Calendar.getInstance().getTime().getTime()));
                    Request persistedRequest = requestDao.save(request);
                    return RequestDTOEntityMapper.getDTOFromEntity(persistedRequest);

                }
                else {

                    if(requestDao.findFirstByRequesterAndActivityAndStatus(request.getRequester(), request.getActivity(), RequestStatus.PENDING) == null) {
                        return addRequest(request);
                    }
                    else{
                        throw new BusinessException("RequestError", "There is an already existing request for this user and this activity!");
                    }
                }
            }
            else if(request.getType() == RequestType.ACCEPT){
                Request existingRequest = requestDao.findFirstByActivityAndStatusOrderByDateAscTimeAsc(request.getActivity(), RequestStatus.PENDING);
                if(existingRequest == null){
                    throw new BusinessException("AcceptRequestError", "There is no existing request for this this activity!");
                }
                else{
                    return acceptRequest(request, existingRequest);
                }
            }
            else{
                if(requestDao.findFirstByRequesterAndActivityAndStatus(request.getRequester(), request.getActivity(), RequestStatus.PENDING) != null) {
                    return denyRequest(request);
                }
                else{
                    throw new BusinessException("DenyRequestError", "There is no existing request for this user and this activity!");
                }
            }
    }

    /**
     * Gets a list of {@link RequestDTO} objects mapping the {@link RequestDTO} objects
     *      corresponding to activities assigned to the user with the
     *      given id persisted in the database
     * @param {@link Integer} user id
     * @throws {@link BusinessException} if the user doesn't exist
     * @return a list of {@link RequestDTO} objects
     * @author Mara Corina
     */
    @Override
    public List<RequestDTO> getAllForUser(Integer id) throws BusinessException {
        Optional<User> user= userDao.findById(id);
        if (!user.isPresent()) {
            throw new BusinessException("Find user error: ", "No user found!");
        }

        List<Integer> userActivitiesIds = this.studentActivityMapperDao.getAllActivitiesIdsByStudentId(id);
        List<Request> userRequests = this.requestDao.findAllByStatusAndActivity_IDIn(RequestStatus.PENDING, userActivitiesIds);

        return RequestDTOEntityMapper.getDTOFromEntityList(userRequests);
    }

    private RequestDTO denyRequest(Request request) {
        request.setStatus(RequestStatus.DENIED);
        Request persistedRequest = requestDao.save(request);
        return RequestDTOEntityMapper.getDTOFromEntity(persistedRequest);
    }

    private RequestDTO acceptRequest(Request request, Request existingRequest) throws BusinessException {
        existingRequest.setStatus(RequestStatus.ACCEPTED);

        // update user didactic activities lists
        Integer requestingUserId = existingRequest.getRequester().getID();
        Integer acceptingUserId = request.getRequester().getID();

        User requestingUser = userDao.findAllByID(requestingUserId);
        User acceptingUser = userDao.findAllByID(acceptingUserId);

        Integer acceptingActivityId = request.getActivity().getID();
        DidacticActivity acceptingActivity = didacticActivityDao.findByID(acceptingActivityId).get();
        DidacticActivity similarDidacticActivity = didacticActivityDao.findByID(existingRequest.getCurrentIdActivity()).get();

        User teacher1 = userDao.findAllByID(acceptingActivity.getTutor().getID());
        User teacher2 = userDao.findAllByID(similarDidacticActivity.getTutor().getID());

        studentActivityMapperDao.updateActivity(acceptingUserId, similarDidacticActivity.getID(), acceptingActivityId);
        studentActivityMapperDao.updateActivity(requestingUserId, acceptingActivityId, similarDidacticActivity.getID());

        if(teacher1.getEmail() != null && !teacher1.getEmail().equals("")) {
            emailManager.sendSimpleMessage(new EmailDTO(teacher1.getEmail(), "Schimb realizat",
                    "Studentul " + acceptingUser.getFirstName() + " " + acceptingUser.getLastName() + " va veni la " +
                            similarDidacticActivity.getCourseName() + " " + similarDidacticActivity.getType() +
                            " cu grupa " + requestingUser.getGrupa() + " " +
                            " in locul studentului " +
                            requestingUser.getLastName() + " " + requestingUser.getFirstName()));
        }

        if(teacher2.getEmail() != null && !teacher1.getEmail().equals("")) {
            emailManager.sendSimpleMessage(new EmailDTO(teacher2.getEmail(), "Schimb realizat",
                    "Studentul/a " + requestingUser.getFirstName() + " " + requestingUser.getLastName() + " va veni la " +
                            similarDidacticActivity.getCourseName() + " tip - " + similarDidacticActivity.getType() +
                            " cu grupa " + acceptingUser.getGrupa() + " " +
                            " in locul studentului/ei " +
                            acceptingUser.getLastName() + " " + acceptingUser.getFirstName() + "."));
        }

        List<Request> getRequests = requestDao.findByRequesterAndCurrentIdActivity(requestingUser, similarDidacticActivity.getID());
        for (Request req: getRequests) {
            if(req.getStatus().equals(RequestStatus.PENDING)) {
                requestDao.delete(req);
            }
        }

        // Save request
        Request persistedRequest = requestDao.save(existingRequest);
        return RequestDTOEntityMapper.getDTOFromEntity(persistedRequest);
    }

    private RequestDTO addRequest(Request request) {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        java.sql.Time time = new java.sql.Time(Calendar.getInstance().getTime().getTime());
        request.setDate(date);
        request.setTime(time);
        request.setStatus(RequestStatus.PENDING);
        Request persistedRequest = requestDao.save(request);
        return RequestDTOEntityMapper.getDTOFromEntity(persistedRequest);
    }

}
