package be.dtoEntityMappers;

import be.dto.RequestDTO;
import be.dto.StudentActivityMapperDTO;
import be.entity.StudentActivityMapper;

public class StudentActivityMapperDTOEntityMapper {

    public static StudentActivityMapper getStudentActivityMapperFromDTOEntity(StudentActivityMapperDTO studentActivityMapperDTO){
        StudentActivityMapper studentActivityMapper = new StudentActivityMapper();
        if(studentActivityMapperDTO != null){
            studentActivityMapper.setId(studentActivityMapperDTO.getID());
            studentActivityMapper.setStudent(UserDTOEntityMapper.getUserFromUserDTO(studentActivityMapperDTO.getStudentDTO()));
            studentActivityMapper.setActivity(DidacticActivityDTOEntityMapper.getDidacticActivityFromDTOEntity(studentActivityMapperDTO.getActivityDTO()));
        }
        return studentActivityMapper;
    }

    public static StudentActivityMapperDTO getDTOFromStudentActivityMapper(StudentActivityMapper studentActivityMapper){
        StudentActivityMapperDTO studentActivityMapperDTO = new StudentActivityMapperDTO();
        if(studentActivityMapper != null){
            studentActivityMapperDTO.setID(studentActivityMapper.getId());
            studentActivityMapperDTO.setStudentDTO(UserDTOEntityMapper.getDTOFromUser(studentActivityMapper.getStudent()));
            studentActivityMapperDTO.setActivityDTO(DidacticActivityDTOEntityMapper.getDidacticActivityDTOFromEntity(studentActivityMapper.getActivity()));
        }
        return studentActivityMapperDTO;
    }
}
