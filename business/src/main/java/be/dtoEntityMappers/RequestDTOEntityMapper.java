package be.dtoEntityMappers;

import be.dto.RequestDTO;
import be.entity.Request;

import java.util.ArrayList;
import java.util.List;


/**
 * Entity Mapper class for {@link Request} & {@link RequestDTO} objects.
 * The class maps an object that has been stated above, to its counterpart.
 *
 * @author Mara Corina
 */
public class RequestDTOEntityMapper {

    public static Request getRequestFromDTOEntity(RequestDTO requestDTO){
        Request request = new Request();
        if(requestDTO != null){
            request.setID(requestDTO.getID());
            request.setType(requestDTO.getType());
            request.setStatus(requestDTO.getStatus());
            request.setDescription(requestDTO.getDescription());
            request.setDate(requestDTO.getDate());
            request.setTime(requestDTO.getTime());
            request.setRequester(UserDTOEntityMapper.getUserFromUserDTO(requestDTO.getRequester()));
            request.setActivity(DidacticActivityDTOEntityMapper.getDidacticActivityFromDTOEntity(requestDTO.getActivity()));
            request.setCurrentIdActivity(requestDTO.getCurrentActivityId());
        }
         return request;
    }

    public static RequestDTO getDTOFromEntity(Request request){
        RequestDTO requestDTO = new RequestDTO();
        if(request != null){
            requestDTO.setID(request.getID());
            requestDTO.setType(request.getType());
            requestDTO.setStatus(request.getStatus());
            requestDTO.setDescription(request.getDescription());
            requestDTO.setDate(request.getDate());
            requestDTO.setTime(request.getTime());
            requestDTO.setRequester(UserDTOEntityMapper.getDTOFromUser(request.getRequester()));
            requestDTO.setActivity(DidacticActivityDTOEntityMapper.getDidacticActivityDTOFromEntity(request.getActivity()));
            requestDTO.setCurrentActivityId(request.getCurrentIdActivity());
        }
        return requestDTO;
    }

    public static List<RequestDTO> getDTOFromEntityList(List<Request> requests){
        List<RequestDTO> requestDTOS = new ArrayList<>();
        for (Request request: requests) {
            requestDTOS.add(getDTOFromEntity(request));
        }
        return requestDTOS;
    }
}

//{
//        "type": "REQUEST",
//        "requester":
//        {
//        "id": "1"
//        },
//        "activity":
//        {
//        "id": "148"
//        }
//}

//{
//        "type": "ACCEPT",
//        "requester":
//        {
//        "id": "1"
//        },
//        "activity":
//        {
//        "id": "148"
//        }
//}
