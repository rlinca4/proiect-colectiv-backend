package be.dtoEntityMappers;

import be.dto.UserDTO;
import be.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import be.entity.Role;

/**
 * Entity Mapper class for {@link User} & {@link UserDTO} objects.
 * The class maps an object that has been stated above, to its counterpart.
 *
 * @author Mara Corina
 */
public class UserDTOEntityMapper {


    private UserDTOEntityMapper(){

    }

    public static User getUserFromUserDTO(UserDTO userDTO){
        User user = new User();
        if (userDTO != null) {
            user.setID(userDTO.getID());
            user.setCounter(userDTO.getCounter());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setMobileNumber(userDTO.getMobileNumber());
            user.setEmail(userDTO.getEmail());
            user.setUsername(userDTO.getUsername());
            user.setPassword(userDTO.getPassword());
            user.setStatus(userDTO.getStatus());
            user.setGrupa(userDTO.getGrupa());
            user.setSemigrupa(userDTO.getSemigrupa());
        }
        return user;
    }
    public static UserDTO getDTOFromUser(User user){
        UserDTO userDTO = new UserDTO();
        if (user != null) {
            userDTO.setCounter(user.getCounter());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setLastName(user.getLastName());
            userDTO.setMobileNumber(user.getMobileNumber());
            userDTO.setEmail(user.getEmail());
            userDTO.setUsername(user.getUsername());
            userDTO.setStatus(user.getStatus());
            userDTO.setID(user.getID());
            userDTO.setGrupa(user.getGrupa());
            userDTO.setSemigrupa(user.getSemigrupa());
            Set<Role> roles = user.getRoles();
            for (Role r: roles) {
                r.setUsers(null);
                r.setPermissions(null);
            }
            userDTO.setRoles(roles);
        }

        return userDTO;
    }
    public static UserDTO getDTOCompleteFromUser(User user) {
        UserDTO userDTO = getDTOFromUser(user);
        if (user != null) {
            userDTO.setPassword(user.getPassword());
        }

        return userDTO;
    }

    /***************************************VIEW_USERS********************************************
     *
     * @param user
     * @return UserDTO without password, with array of roles
     */
    public static UserDTO getDTOWithRolesWithoutPasswFromUser(User user){
        UserDTO userDTO = getDTOFromUser(user);
        userDTO.setPassword("");
        return userDTO;
    }

    /***************************************VIEW_USERS********************************************
     *
     * @param users
     * @return List of UserDTO without passwords and with roles of each
     */
    public static List<UserDTO> getUserDTOListWithRolesWithoutPasswFromUserList(List<User> users){
        List<UserDTO> userDTOList = new ArrayList<>();
        users.forEach(user -> userDTOList.add(getDTOWithRolesWithoutPasswFromUser(user)));
        return userDTOList;
    }

    public static List<UserDTO> getUserDTOListFromUserList(List<User> users){

        List<UserDTO> userDTOList = new ArrayList<>();

        for(User u : users){

            userDTOList.add(getDTOFromUser(u));
        }

        return userDTOList;
    }


    public static List<User> getUserListFromDTOList(List<UserDTO> userDTOList) {

        List<User> userList = new ArrayList<>();

        for (UserDTO dto : userDTOList) {

            userList.add(getUserFromUserDTO(dto));
        }

        return userList;
    }

    public static UserDTO getDTOFromUserWithPass(User user){
        UserDTO userDTO = new UserDTO();
        if (user != null) {
            userDTO.setCounter(user.getCounter());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setLastName(user.getLastName());
            userDTO.setMobileNumber(user.getMobileNumber());
            userDTO.setEmail(user.getEmail());
            userDTO.setUsername(user.getUsername());
            userDTO.setPassword(user.getPassword());
            userDTO.setStatus(user.getStatus());
            userDTO.setID(user.getID());
        }
        return userDTO;
    }

}
