package be.dtoEntityMappers;

import be.dto.DidacticActivityDTO;
import be.entity.DidacticActivity;


/**
 * Entity Mapper class for {@link DidacticActivity} & {@link DidacticActivityDTO} objects.
 * The class maps an object that has been stated above, to its counterpart.
 *
 * @author Mara Corina
 */
public class DidacticActivityDTOEntityMapper {

    public static DidacticActivity getDidacticActivityFromDTOEntity(DidacticActivityDTO didacticActivityDTO){
        DidacticActivity didacticActivity = new DidacticActivity();
        if(didacticActivityDTO != null){
            didacticActivity.setID(didacticActivityDTO.getID());
            didacticActivity.setCourseName(didacticActivityDTO.getCourseName());
            didacticActivity.setType(didacticActivityDTO.getType());
            didacticActivity.setDate(didacticActivityDTO.getDate());
            didacticActivity.setTime(didacticActivityDTO.getTime());
            didacticActivity.setDuration(didacticActivityDTO.getDuration());
            didacticActivity.setRoom(didacticActivityDTO.getRoom());
            didacticActivity.setSpecialization(didacticActivityDTO.getSpecialization());
            didacticActivity.setGrupa(didacticActivityDTO.getGroup());
            didacticActivity.setSemigrupa(didacticActivityDTO.getSemigroup());
            didacticActivity.setStatus(didacticActivityDTO.getStatus());
            didacticActivity.setWeekType(didacticActivityDTO.getWeekType());
            didacticActivity.setDayOfTheWeek(didacticActivityDTO.getDayOfTheWeek());
            didacticActivity.setColor(didacticActivityDTO.getColor());
            didacticActivity.setDescription(didacticActivityDTO.getDescription());
            didacticActivity.setTutor(UserDTOEntityMapper.getUserFromUserDTO(didacticActivityDTO.getTutor()));
        }
        return didacticActivity;
    }

    public static DidacticActivityDTO getDidacticActivityDTOFromEntity(DidacticActivity didacticActivity){
        DidacticActivityDTO didacticActivityDTO = new DidacticActivityDTO();
        if(didacticActivity != null){
            didacticActivityDTO.setID(didacticActivity.getID());
            didacticActivityDTO.setCourseName(didacticActivity.getCourseName());
            didacticActivityDTO.setType(didacticActivity.getType());
            didacticActivityDTO.setDate(didacticActivity.getDate());
            didacticActivityDTO.setTime(didacticActivity.getTime());
            didacticActivityDTO.setDuration(didacticActivity.getDuration());
            didacticActivityDTO.setRoom(didacticActivity.getRoom());
            didacticActivityDTO.setSpecialization(didacticActivity.getSpecialization());
            didacticActivityDTO.setGroup(didacticActivity.getGrupa());
            didacticActivityDTO.setSemigroup(didacticActivity.getSemigrupa());
            didacticActivityDTO.setDayOfTheWeek(didacticActivity.getDayOfTheWeek());
            didacticActivityDTO.setWeekType(didacticActivity.getWeekType());
            didacticActivityDTO.setColor(didacticActivity.getColor());
            didacticActivityDTO.setStatus(didacticActivity.getStatus());
            didacticActivityDTO.setDescription(didacticActivity.getDescription());
            didacticActivityDTO.setTutor(UserDTOEntityMapper.getDTOFromUser(didacticActivity.getTutor()));
        }
        return didacticActivityDTO;
    }
}
