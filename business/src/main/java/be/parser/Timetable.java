package be.parser;

import be.dao.DidacticActivityDao;
import be.dao.RoleDao;
import be.dao.UserDao;
import be.dao.UserRoleDao;
import be.entity.DidacticActivity;
import be.entity.User;
import be.entity.UsersRoles;
import be.entity.types.*;
import be.manager.impl.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

@Component
public class Timetable {
    /**
     * The core information of the timetable
     */
    private String specialization;
    private List<DidacticActivity> allActivities;
    private ActivityDatesHelper activityHelper;
    private UserDao userDao;
    private RoleDao roleDao;
    private UserRoleDao userRoleDao;
    private DidacticActivityDao activityDao;

    @Autowired
    UserManager userManager;


    /**
     * Constructor for the timetable. It parses all the lines, storing the necessary information.
     *
     */
    @Autowired
    public Timetable(UserDao userDao, DidacticActivityDao activityDao) {
        this.userDao = userDao;
        this.activityDao = activityDao;
    }

    public void createTeacherAccounts() throws IOException {
        List<String> htmlCode = ParseURL.getTeachersPageLines();
        List<List<String>> rows = divideIntoRows(htmlCode);
        rows.forEach(this::CreatePersistTeacher);
    }

    public void start () throws IOException {
        ParseURL website = new ParseURL();
        allActivities = new ArrayList<>();
        specialization = website.getSpecialization();
        activityHelper = new ActivityDatesHelper(activityDao);

        List<String> htmlCode = website.getLines();

        List<List<String>> rows = divideIntoRows(htmlCode);

        ActivityDatesHelper finalActivityHelper = activityHelper;
        rows.forEach(row -> {
            DidacticActivity activity = processRow(row);

            if(activity != null) {
                activity.setSpecialization(specialization);

                allActivities.add(activity);
                finalActivityHelper.Persist(activity, website.getSemester());
            }
        });
    }

    private List<List<String>> divideIntoRows(List<String> table) {
        Pattern beginningOfRow = Pattern.compile("<tr .*>");
        Pattern endOfRow = Pattern.compile("</tr>");

        List<List<String>> rows = new ArrayList<>();
        List<String> currentRow = new ArrayList<>();

        boolean inRow = false;
        for (String line : table) {
            Matcher endOfRowMatcher = endOfRow.matcher(line);
            if (endOfRowMatcher.matches()) {
                rows.add(currentRow);
                currentRow = new ArrayList<>();
                inRow = false;
            }

            if (inRow) {
                currentRow.add(line);
            } else {
                Matcher beginningOfRowMatcher = beginningOfRow.matcher(line);
                if (beginningOfRowMatcher.matches()) {
                    inRow = true;
                }
            }
        }

        //The first row is the header of the table. Given that it does not contain any useful
        //information, it should be ignored
        return rows.subList(1, rows.size());
    }

    private DidacticActivity processRow(List<String> row) {
        DidacticActivity activity = new DidacticActivity();
        int currentColumn = 0;

        for (String column : row) {
            Pattern informationPattern = Pattern.compile("<td.*>([^<]+)<.*");
            Matcher matcher = informationPattern.matcher(column);
            if (matcher.find()) {
                String information = matcher.group(1);
                SetInformation(activity, currentColumn, information);
            }
            else{
                return null;
            }
            currentColumn++;
        }
        return activity;
    }

    private void CreatePersistTeacher(List<String> row){
        for (String column : row) {
            Pattern informationPattern = Pattern.compile("<td.*>([^<]+)<.*");
            Matcher matcher = informationPattern.matcher(column);
            if (matcher.find()) {
                String information = matcher.group(1);
                User teacher = SetTeacherInfo(information);
                if (teacher == null) return;
                if(userDao.findUserByFirstNameAndLastName(teacher.getFirstName(), teacher.getLastName()) != null) return;
                User persisted = userDao.save(teacher);
                System.out.println("Added " + teacher);
            }
        }
    }

    private User SetTeacherInfo(String information) {
        User teacher = new User();
        String[] a = information.split(" ");
        if(a.length < 2) return null;
        teacher.setLastName(a[1]);
        teacher.setFirstName(a[2]);
        teacher.setCounter(0);
        teacher.setMobileNumber("");
        teacher.setEmail("");
        teacher.setStatus(1);
        teacher.setUsername(userManager.generateUsername(teacher.getFirstName(), teacher.getLastName()));
        teacher.setPassword("");
        return teacher;
    }

    private void SetInformation(DidacticActivity activity, int currentColumn, String information) {
        switch (currentColumn) {
            case 0:
                activityHelper.setDay(information);
                break;
            case 1:
                String[] hours = information.split("-");
                try {
                    activity.setTime(new java.sql.Time(new SimpleDateFormat("HH").parse(hours[0]).getTime()));
                    activity.setDuration(parseInt(hours[1]) - parseInt(hours[0]));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                activityHelper.setFrequency(information);
                break;
            case 3:
                activity.setRoom(information);
                break;
            case 4:
                try {
                    activity.setGrupa(parseInt(information.split("/")[0]));
                    activity.setSemigrupa(parseInt(information.split("/")[1]));
                } catch (Exception ignored) {
                }
                break;
            case 5:
                switch (information) {
                    case "Curs":
//                        activityDao.findByCourseNameAndSpecialization()
                        activity.setType(DidacticActivityType.COURSE);
                        break;
                    case "Seminar":
                        activity.setType(DidacticActivityType.SEMINARY);
                        break;
                    case "Laborator":
                        activity.setType(DidacticActivityType.LABORATORY);
                        break;
                }
                break;
            case 6:
                activity.setCourseName(information);
                break;
            case 7:
                String[] name = information.split(" ");
                User found = userDao.findUserByFirstNameAndLastName(name[2], name[1]);
                if(found == null){
                    found = userDao.findAllByID(0);
                }
                activity.setTutor(found);
                break;
            default:
                throw new IllegalArgumentException("Error parsing activity! Number is " + currentColumn);
        }
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (DidacticActivity activity : allActivities) {
            output.append(activity);
        }
        return output.toString();
    }
}

class ActivityDatesHelper {

    private DidacticActivityDao activityDao;

    private String day;
    private String frequency;

    ActivityDatesHelper(DidacticActivityDao activityDao) {
        this.activityDao = activityDao;
    }

    void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    void setDay(String day) {
        this.day = day;
    }

    void Persist(DidacticActivity activity, int semester) {
        String startingDate = SemesterInfo.getStartingDate(semester);
        LocalDate date;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            date = dateFormat.parse(startingDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            date = date.plusDays(FromDayToInteger.getInteger(day));
            activity.setDate(Date.valueOf(date));
        } catch (ParseException e) {
            // ... handle parsing exception
        }

        if (frequency.equals("sapt. 2"))
            activity.setWeekType(WeekType.ODD);
        else if(frequency.equals("sapt. 1"))
            activity.setWeekType(WeekType.EVEN);

        activity.setDayOfTheWeek(DayOfTheWeek.values()[FromDayToInteger.getInteger(day)]);

        activity.setColor(DidacticActivityColor.GREEN);

        if(activity.getType() == DidacticActivityType.COURSE) {
            List<DidacticActivity> didacticActivity = activityDao.findByCourseName(activity.getCourseName());
            if(didacticActivity.size() == 0)
            {
                activityDao.save(activity);
            }
        }
        else {
            activityDao.save(activity);
        }
    }
}