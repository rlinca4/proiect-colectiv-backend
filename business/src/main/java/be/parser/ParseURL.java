package be.parser;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParseURL {
    public static final String HTML_CADRE_2019 = "http://www.cs.ubbcluj.ro/files/orar/2019-1/cadre/index.html";
    public static final String ORAR_IR_2019 = "http://www.cs.ubbcluj.ro/files/orar/2019-1/tabelar/I3.html";
    /**
     * Holds the url to the timetable.
     */
    private final URL website;

    /**
     * Contains the HTML lines of website.
     */
    private List<String> lines = null;

    /**
     * Constructor for ParseURL
     *
     * @throws MalformedURLException
     */

    public ParseURL() throws MalformedURLException {
        website = new URL(ORAR_IR_2019);
    }

    /**
     * Reads and stores the source code of the website
     *
     * @throws IOException
     */
    private void readHtmlLines() throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(website.openStream()))) {
            lines = reader.lines()
                    .filter(string -> !string.isEmpty())
                    .collect(Collectors.toList());
        }
    }

    /**
     * Getter for the source code of the website
     *
     * @return a list of all the lines
     * @throws IOException
     */
    List<String> getLines() throws IOException {
        if (lines == null) {
            readHtmlLines();
        }
        return lines;
    }

    public static List<String> getTeachersPageLines() throws IOException {
        URL site = new URL(HTML_CADRE_2019);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(site.openStream()))) {
            return reader.lines()
                    .filter(string -> !string.isEmpty())
                    .collect(Collectors.toList());
        }
    }

    /**
     * Determines the semester of the timetable from the url
     *
     * @return 1 if the timetable is for the first semester and 2 otherwise
     */
    public int getSemester() {
        String[] parts = website.getPath().split("-");
        return parts[1].charAt(0) - '0';
    }

    /**
     * Gets the specialization from the url
     *
     * @return the identifier for the specialization
     */
    String getSpecialization() {
        String url = website.toString();
        url = url.substring(url.lastIndexOf("/") + 1);
        return url.split("\\.")[0];
    }
}
