-- /**
--  *
--  * @author Mara Corina
--  */

-- Insert roles --
INSERT INTO roles(ID, type) VALUES (1, 'ADMINISTRATOR');
INSERT INTO roles(ID, type) VALUES (2, 'TEACHER');
INSERT INTO roles(ID, type) VALUES (3, 'STUDENT');

-- Insert permissions --
INSERT INTO permissions(ID, description, type) VALUES (1, 'edit activities status, ', 'ACTIVITY_MANAGEMENT');
INSERT INTO permissions(ID, description, type) VALUES (2, 'close activity', 'ACTIVITY_CLOSE');
INSERT INTO permissions(ID, description, type) VALUES (3, 'edit permissions', 'PERMISSION_MANAGEMENT');
INSERT INTO permissions(ID, description, type) VALUES (4, 'edit users', 'USER_MANAGEMENT');

-- Insert roles_permissions --
INSERT INTO roles_permissions (id_role, id_permission) VALUES (1, 1);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (1, 2);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (1, 3);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (1, 4);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (2, 1);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (2, 2);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (3, 1);
INSERT INTO roles_permissions (id_role, id_permission) VALUES (4, 1);

-- Insert users --
-- every password inserted represents "admin" value after sha256 hash --
INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, grupa, semigrupa)
VALUES (0, 0, 'admin', 'admin', '0743170363', 'admin@scs.ubbcluj.ro', 'admin', 'admin', 1, 'INFO_ROMANA', 111, 1);
--
INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
VALUES (1, 0, 'Corina', 'Mara', '0743170363', 'mcir2339@scs.ubbcluj.ro', 'mcir2339', 'parola', 1, 'INFO_ROMANA', 234, 2);

INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
VALUES (2, 0, 'Razvan', 'Linca', '0721946737', 'lcir2331@scs.ubbcluj.ro', 'lcir2331', 'parola', 1, 'INFO_ROMANA', 234, 2);

INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
VALUES (3, 0, 'Bogdan', 'Luca', '0767020677', 'lbir2332@scs.ubbcluj.ro', 'lbir2332', 'parola', 1, 'INFO_ROMANA', 234, 2);

INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
VALUES (4, 0, 'Sergiu', 'Harjoc', '0757725517', 'hsir2311@scs.ubbcluj.ro', 'hsir2311', 'parola', 1, 'INFO_ROMANA', 234, 1);

INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
VALUES (5, 0, 'Laurentiu', 'Ile', '0752121211', 'ilir2322@scs.ubbcluj.ro', 'ilir2322', 'parola', 1, 'INFO_ROMANA', 234, 1);

INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
VALUES (6, 0, 'Lisa', 'Munteanu', '0767020677', 'mlir2357@scs.ubbcluj.ro', 'mlir2357', 'parola', 1, 'INFO_ROMANA', 235, 2);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
-- VALUES (7, 0, 'Miruna', 'Dinu', '0770741299', 'dmig0162@scs.ubbcluj.ro', 'dmig0162', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, 'INFO_GERMANA', 731, 2);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
-- VALUES (8, 0, 'Dorin', 'Chis', '0787345025', 'cdig0156@scs.ubbcluj.ro', 'cdig0156', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, 'INFO_GERMANA', 731, 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
-- VALUES (9, 0, 'Antonia', 'Grosan', '0758081166', 'gaie2326@scs.ubbcluj.ro', 'gaie2326', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, 'INFO_ENGLEZA', 933, 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (10, 0, 'Dan', 'Suciu', '0750000000', 'tzutzu@cs.ubbcluj.ro', 'tzutzu', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (11, 0, 'Virginia', 'Niculescu', '0750000000', 'vniculescu@cs.ubbcluj.ro', 'vniculescu', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (12, 0, 'Dana', 'Lupsa', '0750000000', 'dana@cs.ubbcluj.ro', 'dana', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (13, 0, 'Ioan', 'Lazar', '0750000000', 'ilazar@cs.ubbcluj.ro', 'ilazar', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (14, 0, 'Andreea', 'Pop', '0750000000', 'mihis@cs.ubbcluj.ro', 'mihis', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (15, 0, 'Ioan', 'Sima', '0750000000', 'sima.ioan@cs.ubbcluj.ro', 'sima.ioan', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (16, 0, 'Daniel', 'Bota', '0750000000', 'dbota@cs.ubbcluj.ro', 'dbota', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (17, 0, 'Iulian', 'Benta', '0750000000', 'benta@cs.ubbcluj.ro', 'benta', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (18, 0, 'Iulian', 'Simion', '0750000000', 'isimion@cs.ubbcluj.ro', 'isimion', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (19, 0, 'Simona', 'Motogna', '0750000000', 'motogna@cs.ubbcluj.ro', 'motogna', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (20, 0, 'Vladiela', 'Petrascu', '0750000000', 'vladi@cs.ubbcluj.ro', 'vladi', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status)
-- VALUES (21, 0, 'Dan', 'Cojocar', '0750000000', 'dan@cs.ubbcluj.ro', 'dan', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
-- VALUES (22, 0, 'Ana', 'Maria', '0750000000', 'ana@cs.ubbcluj.ro', 'ana', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, 'INFO_ROMANA', 236, 1);
--
-- INSERT INTO users(ID, counter, first_name, last_name, mobile_number, email, username, password, status, specialization, 'grupa', semigrupa)
-- VALUES (23, 0, 'Lucian', 'Dobrin', '0750000000', 'lucian@cs.ubbcluj.ro', 'lucian', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, 'INFO_ROMANA', 237, 1);

-- Insert into student_activity_mapper --
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
--     VALUES (1, 2552);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
--                 VALUES (1, 2557);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (1, 512);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (1, 513);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (2, 510);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (2, 512);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (2, 520);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (22, 685);
-- INSERT INTO student_activity_mapper(ID_STUDENT, ID_ACTIVITY)
-- VALUES (23, 743);
--
-- -- Insert user_roles --
-- INSERT INTO users_roles(id_role, id_user) VALUES (0, 1);
-- INSERT INTO users_roles(id_role, id_user) VALUES (1, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (2, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (3, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (4, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (5, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (6, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (7, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (8, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (9, 5);
-- INSERT INTO users_roles(id_role, id_user) VALUES (10, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (11, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (12, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (13, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (14, 3);
-- INSERT INTO users_roles(id_role, id_user) VALUES (14, 4);
-- INSERT INTO users_roles(id_role, id_user) VALUES (15, 4);
-- INSERT INTO users_roles(id_role, id_user) VALUES (16, 4);
-- INSERT INTO users_roles(id_role, id_user) VALUES (17, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (17, 3);
-- INSERT INTO users_roles(id_role, id_user) VALUES (17, 4);
-- INSERT INTO users_roles(id_role, id_user) VALUES (18, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (19, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (20, 3);
-- INSERT INTO users_roles(id_role, id_user) VALUES (21, 2);
-- INSERT INTO users_roles(id_role, id_user) VALUES (21, 4);


-- Insert didactic_activities --
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-11-22', '18:00', '2/I', 13, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-11-29', '18:00', '2/I', 13, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-06', '18:00', '2/I', 13, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-13', '18:00', '2/I', 13, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-20', '18:00', '2/I', 13, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-27', '18:00', '2/I', 13, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2020-01-03', '18:00', '2/I', 13, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-11-18', '12:00', '6/II', 12, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-11-25', '12:00', '6/II', 12, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-02', '12:00', '6/II', 12, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-09', '12:00', '6/II', 12, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-16', '12:00', '6/II', 12, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-23', '12:00', '6/II', 12, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-30', '12:00', '6/II', 12, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-11-18', '14:00', '6/II', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-11-25', '14:00', '6/II', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-12-02', '14:00', '6/II', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-12-09', '14:00', '6/II', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-12-16', '14:00', '6/II', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-12-23', '14:00', '6/II', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'COURSE', '2019-12-30', '14:00', '6/II', 11, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-11-18', '08:00', 'C310', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-11-25', '08:00', 'C310', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-02', '08:00', 'C310', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-09', '08:00', 'C310', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-16', '08:00', 'C310', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-23', '08:00', 'C310', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-30', '08:00', 'C310', 18, 'INFO_GERMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-11-18', '10:00', 'C310', 17, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-11-25', '10:00', 'C310', 17, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-02', '10:00', 'C310', 17, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-09', '10:00', 'C310', 17, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-16', '10:00', 'C310', 17, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-23', '10:00', 'C310', 17, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'COURSE', '2019-12-30', '10:00', 'C310', 17, 'INFO_GERMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-11-18', '10:00', '2/I', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-11-25', '10:00', '2/I', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-02', '10:00', '2/I', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-09', '10:00', '2/I', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-16', '10:00', '2/I', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-23', '10:00', '2/I', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'COURSE', '2019-12-30', '10:00', '2/I', 19, 'INFO_ENGLEZA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-11-20', '14:00', 'C512', 234, 'ALL_CHANGES', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-11-27', '14:00', 'C512', 234, 'ALL_CHANGES', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-12-04', '14:00', 'C512', 234, 'ALL_CHANGES', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-12-11', '14:00', 'C512', 234, 'ALL_CHANGES', 11, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-22', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-29', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-06', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-13', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-20', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-27', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2020-01-03', '10:00', 'A313', 234, 'ALL_CHANGES', 14, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-20', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-27', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-04', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-11', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-18', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-25', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2020-01-01', '12:00', 'A323', 721, 'ALL_CHANGES', 18, 'INFO_GERMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-19', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-26', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-03', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-10', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-17', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-24', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-31', '14:00', 'C514', 933, 'ALL_CHANGES', 19, 'INFO_ENGLEZA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-11-27', '16:00', 'C512', 235, 'ALL_CHANGES', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-12-04', '16:00', 'C512', 235, 'ALL_CHANGES', 11, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'SEMINARY', '2019-12-11', '16:00', 'C512', 235, 'ALL_CHANGES', 11, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-20', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-11-27', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-04', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-11', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-18', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2019-12-25', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'SEMINARY', '2020-01-01', '18:00', 'A310', 235, 'ALL_CHANGES', 14, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-19', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-26', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-03', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-10', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-17', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-24', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-31', '16:00', 'L321', 234, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-20', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-27', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-04', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-11', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-18', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-25', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2020-01-01', '08:00', 'L002', 234, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-21', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-28', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-05', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-12', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-19', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-26', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2020-01-02', '14:00', 'L302', 235, 1, 'ALL_CHANGES', 14, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-22', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-11-29', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-06', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-13', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-20', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2019-12-27', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Limbaje formale si tehnici de compilare', 'LABORATORY', '2020-01-03', '10:00', 'L336', 235, 2, 'ALL_CHANGES', 14, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-19', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-26', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-03', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-10', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-17', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-24', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-31', '18:00', 'L001', 234, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-20', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-27', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-04', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-11', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-18', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-25', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2020-01-01', '08:00', 'L002', 234, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-22', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-29', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-06', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-13', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-20', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-27', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2020-01-03', '08:00', 'L002', 235, 2, 'NO_CHANGES', 15, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-22', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-29', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-06', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-13', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-20', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-27', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2020-01-03', '10:00', 'L001', 235, 1, 'NO_CHANGES', 15, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-22', '12:00', 'L337', 234, 2, 'NO_CHANGES', 16, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-29', '12:00', 'L337', 234, 2, 'NO_CHANGES', 16, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-06', '12:00', 'L337', 234, 2, 'NO_CHANGES', 16, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-13', '12:00', 'L337', 234, 2, 'NO_CHANGES', 16, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-22', '12:00', 'L337', 234, 1, 'ALL_CHANGES', 16, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-11-29', '12:00', 'L337', 234, 1, 'ALL_CHANGES', 16, 'INFO_ROMANA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare paralela si distribuita', 'LABORATORY', '2019-12-06', '12:00', 'L337', 234, 1, 'ALL_CHANGES', 16, 'INFO_ROMANA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-11-20', '16:00', 'L402', 933, 1, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-11-27', '16:00', 'L402', 933, 1, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-12-04', '16:00', 'L402', 933, 1, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
--
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-11-20', '16:00', 'L402', 933, 2, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-11-27', '16:00', 'L402', 933, 2, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-12-04', '16:00', 'L402', 933, 2, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
-- INSERT INTO didactic_activities (course_name, type, date, time, room, 'grupa', semigrupa, status, id_tutor, specialization) VALUES ('Programare pentru dispozitive mobile', 'LABORATORY', '2019-12-11', '16:00', 'L402', 933, 2, 'ALL_CHANGES', 21, 'INFO_ENGLEZA');
