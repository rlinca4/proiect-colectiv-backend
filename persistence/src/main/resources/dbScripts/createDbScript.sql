-- /*
--  *
--  * @author Razvan Cosmin Linca
--  */
CREATE TABLE student_activity_mapper
(
    id INTEGER IDENTITY(1,1),
    id_student INTEGER,
    id_activity INTEGER,
    CONSTRAINT FK_student FOREIGN KEY (id_student) REFERENCES users(ID),
    CONSTRAINT FK_activity FOREIGN KEY (id_activity) REFERENCES didactic_activities(ID)
);

CREATE TABLE tokens
(
    ID INTEGER IDENTITY(1,1) PRIMARY KEY,
    token NVARCHAR(100) NOT NULL,
    username NVARCHAR(50) NOT NULL
);

-- /**
--  *
--  * @author Mara Corina
--  */
CREATE TABLE users
(
    ID INTEGER IDENTITY(1,1) PRIMARY KEY,
    counter INTEGER DEFAULT 0,
    first_name NVARCHAR(50) NOT NULL,
    last_name NVARCHAR(50) NOT NULL,
    mobile_number NVARCHAR(50) NOT NULL,
    email NVARCHAR(50) NOT NULL,
    username NVARCHAR(50) NOT NULL,
    password NVARCHAR(50) NOT NULL,
    status INTEGER DEFAULT 1,
    specialization NVARCHAR(50) DEFAULT NULL,
    grupa INTEGER DEFAULT NULL,
    semigrupa INTEGER DEFAULT NULL
);

CREATE TABLE didactic_activities
(
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    course_name NVARCHAR(50) NOT NULL ,
    type NVARCHAR(50) NOT NULL,
    date DATE DEFAULT NULL,
    time TIME DEFAULT NULL,
    duration INTEGER DEFAULT 2,
    room NVARCHAR(50),
    specialization NVARCHAR(50) DEFAULT NULL,
    grupa INTEGER DEFAULT NULL,
    semigrupa INTEGER DEFAULT NULL,
    status NVARCHAR(50) DEFAULT NULL,
    week_type NVARCHAR(50) DEFAULT NULL,
    day_of_the_week NVARCHAR(50) DEFAULT NULL,
    color NVARCHAR(50) DEFAULT NULL,
    description varchar(255) DEFAULT NULL,
    id_tutor INTEGER NOT NULL,
    unique (course_name, grupa, semigrupa),
    CONSTRAINT FK_activity_user_assigned FOREIGN KEY (id_tutor) REFERENCES users(ID)
        ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE requests
(
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    type NVARCHAR(50),
    status NVARCHAR(50) DEFAULT 'PENDING',
    description varchar(255) DEFAULT NULL,
    date DATE NOT NULL,
    time TIME NOT NULL,
    id_requester INTEGER NOT NULL,
    id_activity INTEGER DEFAULT NULL,
    current_id_activity INTEGER,
    CONSTRAINT FK_request_user_assigned FOREIGN KEY (id_requester) REFERENCES users(ID),
    CONSTRAINT FK_request_activity_assigned FOREIGN KEY(id_activity) REFERENCES didactic_activities(ID)
);

CREATE TABLE roles (
                       ID INTEGER PRIMARY KEY AUTOINCREMENT,
                       type NVARCHAR(50) NOT NULL
);

CREATE TABLE users_roles (
                             id INTEGER IDENTITY(1,1),
                             id_user INTEGER NOT NULL,
                             id_role INTEGER NOT NULL,
                             PRIMARY KEY (id_user, id_role),
                             CONSTRAINT FK_users_roles_role_id FOREIGN KEY (id_role) REFERENCES roles(ID),
                             CONSTRAINT FK_users_roles_user_id FOREIGN KEY (id_user) REFERENCES users(ID)
);

CREATE TABLE permissions (
                             ID INTEGER PRIMARY KEY AUTOINCREMENT,
                             description varchar(255) DEFAULT NULL,
                             type varchar(50) NOT NULL
);

CREATE TABLE roles_permissions (
                                   id_role INTEGER NOT NULL,
                                   id_permission INTEGER NOT NULL,
                                   PRIMARY KEY (id_role, id_permission),
                                   CONSTRAINT FK_roles_permissions_permission_id FOREIGN KEY (id_permission) REFERENCES permissions(ID),
                                   CONSTRAINT FK_roles_permissions_role_id FOREIGN KEY (id_role) REFERENCES roles(ID)
);

CREATE TABLE notifications (
                               ID INTEGER PRIMARY KEY AUTOINCREMENT,
                               date datetime DEFAULT NULL,
                               message varchar(255) DEFAULT NULL,
                               type varchar(50) NOT NULL,
                               url varchar(255) DEFAULT NULL,
                               id_user INTEGER DEFAULT NULL,
                               id_activity INTEGER DEFAULT NULL,
                               CONSTRAINT FK_user_assigned FOREIGN KEY (id_user) REFERENCES users(ID) ON DELETE NO ACTION ON UPDATE NO ACTION,
                               CONSTRAINT FK_activity_assigned FOREIGN KEY (id_activity) REFERENCES didactic_activities(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
);