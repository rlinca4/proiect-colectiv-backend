package be.dao;

import be.entity.DidacticActivity;
import be.entity.types.DidacticActivityColor;
import be.entity.types.DidacticActivityStatus;
import be.entity.types.DidacticActivityType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface DidacticActivityDao extends CrudRepository<DidacticActivity, Integer> {
    DidacticActivity save(DidacticActivity didacticActivity);
    DidacticActivity findByCourseNameAndSpecialization(String courseName,String specialization);
    List<DidacticActivity> getDidacticActivitiesByTutorID(Integer id);
    List<DidacticActivity> findByCourseName(String courseName);

    @Query("FROM DidacticActivity da where course_name = :#{#activity.courseName} " +
            "AND type = :#{#activity.type} " +
            "AND specialization = :#{#activity.specialization} " +
            "AND grupa = :grupa " +
            "AND (semigrupa = :semigrupa OR semigrupa is NULL)")
    DidacticActivity getSimilarDidacticActivity(@Param("activity") DidacticActivity activity,
                                                @Param("grupa") Integer grupa,
                                                @Param("semigrupa") Integer semigrupa);


    @Query("FROM DidacticActivity da where course_name = :courseName " +
            "AND grupa = :grupa " +
            "AND (semigrupa = :semigrupa OR semigrupa is NULL)")
    List<DidacticActivity> getCorrespondingActivitiesByCourse(@Param("courseName") String courseName,
                                                @Param("grupa") Integer grupa,
                                                @Param("semigrupa") Integer semigrupa);

    @Query("FROM DidacticActivity da WHERE " +
            "ID <> :idActivity AND " +
            "course_name = :courseName AND " +
            "type = :activityType AND " +
            "(color = 'YELLOW' OR color = 'GREEN')")
    List<DidacticActivity> findActivitiesForChange(@Param("idActivity") Integer idActivity,
                                                   @Param("courseName") String courseName,
                                                   @Param("activityType") DidacticActivityType didacticActivityType);

    Optional<DidacticActivity> findByID(Integer activityId);

    @Transactional
    @Modifying
    @Query("UPDATE DidacticActivity set status = :status, " +
            "color = :color " +
            "where ID = :idActivity")
    void update(@Param("idActivity") Integer idActivity,
                @Param("status")DidacticActivityStatus status,
                @Param("color") DidacticActivityColor color);

    @Query("SELECT distinct courseName FROM DidacticActivity")
    List<String> getAllCourses();

    @Query("FROM DidacticActivity " +
            "WHERE type = 'COURSE'")
    List<DidacticActivity> getAllCoursesWithId();


}
