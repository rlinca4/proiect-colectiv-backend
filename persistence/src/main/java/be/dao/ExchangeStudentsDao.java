package be.dao;

import be.entity.ExchangeStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeStudentsDao extends JpaRepository<ExchangeStudent, Integer> {
    ExchangeStudent findByIDUserAndIDDidacticActivity(Integer idUser, Integer idDidacticActivity);
    ExchangeStudent findByIDDidacticActivity(Integer idDidacticActivity);

}
