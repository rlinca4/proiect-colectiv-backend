package be.dao;

import be.entity.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Data Access Object class for {@link User} objects.
 * It has direct access to the database and all {@link User} related tables.
 *
 * @author Mara Corina
 * @author Razvan Cosmin Linca
 */
@Repository
public interface UserDao extends CrudRepository<User, Integer> {

    User findAllByID(Integer id);

    List<User> findAll();

    User findUserByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    User findUserByFirstNameAndLastName(String firstName, String lastName);

    @Query("SELECT DISTINCT da.grupa FROM DidacticActivity da")
    List<Integer> getAllGroups();

    User findByUsernameAndStatus(String username, Integer status);

    @Transactional
    @Modifying
    @Query("UPDATE User u SET mobile_number = :mobileNumber," +
            "email = :email, " +
            "password = :password, " +
            "status = :status " +
            "WHERE ID = :id")
    int update(@Param("mobileNumber") String mobileNumber,
               @Param("email") String email,
               @Param("password") String password,
               @Param("status") Integer status,
               @Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("UPDATE User u " +
            "SET password = :newPassword " +
            "WHERE ID = :userId")
    int updatePassword(@Param("userId") Integer userId,
                       @Param("newPassword") String newPassword);
}
