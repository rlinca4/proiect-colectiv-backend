package be.dao;

import be.entity.DidacticActivity;
import be.entity.StudentActivityMapper;
import be.entity.User;
import be.entity.types.DidacticActivityType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Data Access Object class for {@link StudentActivityMapperDao} objects.
 * It has direct access to the database and all {@link StudentActivityMapperDao} related tables.
 *
 * @author Razvan Cosmin Linca
 */
@Repository
public interface StudentActivityMapperDao extends CrudRepository<StudentActivityMapper,  Integer> {

    List<StudentActivityMapper> findAll();
    List<StudentActivityMapper> findByStudentIDAndActivityID(Integer studentId, Integer activityId);

    @Transactional
    @Modifying
    void deleteByStudentAndActivity(User student, DidacticActivity activity);

    @Query("SELECT d FROM DidacticActivity d" +
            " INNER JOIN StudentActivityMapper m ON m.activity = d.ID" +
            " INNER JOIN User u ON m.student = u.ID" +
            " WHERE id_student = :idStudent")
    List<DidacticActivity> findAllByStudentId(@Param("idStudent") Integer idStudent);

    @Transactional
    @Modifying
    @Query("UPDATE StudentActivityMapper sam SET id_activity = :idActivity " +
            "WHERE id_student = :idStudent AND " +
            "id_activity = :oldId")
    void updateActivity(@Param("idStudent") Integer idStudent,
                        @Param("idActivity") Integer idActivity,
                        @Param("oldId") Integer oldId);


    @Query("SELECT d.ID FROM DidacticActivity d" +
            " INNER JOIN StudentActivityMapper m ON m.activity = d.ID" +
            " INNER JOIN User u ON m.student = u.ID" +
            " WHERE id_student = :idStudent")
    List<Integer> getAllActivitiesIdsByStudentId(@Param("idStudent") Integer idStudent);
}
