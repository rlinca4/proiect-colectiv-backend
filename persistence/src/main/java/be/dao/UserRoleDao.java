package be.dao;

import be.entity.User;
import be.entity.UsersRoles;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.*;

public interface UserRoleDao extends CrudRepository<UsersRoles, Integer> {

    @Query("FROM UsersRoles where id_user = :id")
    List<UsersRoles> findByUserId(@Param("id") Integer id);
}
