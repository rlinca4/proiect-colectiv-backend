package be.dao;

import be.entity.DidacticActivity;
import be.entity.Request;
import be.entity.User;
import be.entity.types.RequestStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Data Access Object class for {@link Request} objects.
 * It has direct access to the database and all {@link Request} related tables.
 *
 * @author Mara Corina
 */
@Repository
public interface RequestDao extends CrudRepository<Request, Integer> {

    Request findAllByID(Integer id);

    List<Request> findAll();

    Request findFirstByRequesterAndActivityAndStatus(User user, DidacticActivity didacticActivity, RequestStatus requestStatus);

    Request findFirstByActivityAndStatusOrderByDateAscTimeAsc(DidacticActivity didacticActivity, RequestStatus requestStatus);

    List<Request> findAllByStatusAndActivity_Grupa(RequestStatus status, Integer grupa);

    List<Request> findAllByStatusAndActivity_IDIn(RequestStatus status, List<Integer> activitiesIds);

    List<Request> findByRequesterAndCurrentIdActivity(User requester, Integer currentIdActivity);
}
