package be.dao;

import be.entity.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Razvan Cosmin Linca
 */
@Repository
public interface TokenDao extends CrudRepository<Token, Integer> {
}
