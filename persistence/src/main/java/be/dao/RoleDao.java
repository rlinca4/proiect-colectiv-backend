package be.dao;

import be.entity.Role;
import be.entity.types.RoleType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Data Access Object class for {@link Role} objects.
 * It has direct access to the database and all {@link Role} related tables.
 *
 * @author Mara Corina
 */
@Repository
public interface RoleDao extends CrudRepository<Role, Integer> {

    Role findByType(RoleType type);

    Role findByID(Integer id);
}
