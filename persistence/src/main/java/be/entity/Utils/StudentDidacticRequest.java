package be.entity.Utils;

import java.io.Serializable;

/**
 *
 * @author Mara Corina
 */
public class StudentDidacticRequest implements Serializable {
    private Integer userId;
    private Integer activityId;

    public StudentDidacticRequest() {
    }

    public StudentDidacticRequest(Integer userId, Integer activityId) {
        this.userId = userId;
        this.activityId = activityId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }
}
