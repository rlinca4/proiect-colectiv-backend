package be.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Razvan Cosmin Linca
 */
@Entity
@Table(name = "tokens")
public class Token implements Serializable {
    @Id
    @GeneratedValue(generator="sqlite")
    @TableGenerator(name="sqlite", table="sqlite_sequence",
            pkColumnName="name", valueColumnName="seq",
            pkColumnValue="ID")
    private Integer ID;

    @Column(name = "token")
    private String token;

    @Column(name = "username")
    private String username;

    public Token(String token, String username) {
        this.token = token;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
