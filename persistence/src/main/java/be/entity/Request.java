package be.entity;

import be.entity.types.RequestStatus;
import be.entity.types.RequestType;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author Mara Corina
 */
@Entity
@Table(name = "requests")
@NamedQueries({
})
public class Request implements Serializable {

    @Id
    @GeneratedValue(generator="sqlite")
    @TableGenerator(name="sqlite", table="sqlite_sequence",
            pkColumnName="name", valueColumnName="seq",
            pkColumnValue="ID")
    private Integer ID;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private RequestType type;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private RequestStatus status;

    @Column(name = "description")
    private String description;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_requester", referencedColumnName = "ID")
    private User requester;

    @Column(name = "current_id_activity")
    private Integer currentIdActivity;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_activity",referencedColumnName = "ID")
    private DidacticActivity activity;


    public Request(Integer ID,RequestType type, String description, Date date, Time time) {
        this.ID = ID;
        this.type = type;
        this.description = description;
        this.date = date;
        this.time = time;
    }

    public Request(RequestType type, RequestStatus status, String description, Date date, Time time, User requester, DidacticActivity activity) {
        this.type = type;
        this.status = status;
        this.description = description;
        this.date = date;
        this.time = time;
        this.requester = requester;
        this.activity = activity;
    }

    public Request() {
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    public DidacticActivity getActivity() {
        return activity;
    }

    public void setActivity(DidacticActivity activity) {
        this.activity = activity;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public Integer getCurrentIdActivity() {
        return currentIdActivity;
    }

    public void setCurrentIdActivity(Integer currentIdActivity) {
        this.currentIdActivity = currentIdActivity;
    }
}
