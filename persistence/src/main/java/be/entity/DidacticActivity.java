package be.entity;

import be.entity.types.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Mara Corina
 */
@Entity
@Table(name = "didactic_activities")
@NamedQueries({
})
public class DidacticActivity implements Serializable {

    @Id
    @GeneratedValue(generator="sqlite")
    @TableGenerator(name="sqlite", table="sqlite_sequence",
            pkColumnName="name", valueColumnName="seq",
            pkColumnValue="ID")
    private Integer ID;

    @Column(name = "course_name")
    private String courseName;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private DidacticActivityType type;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "room")
    private String room;

    @Column(name = "specialization")
    private String specialization;

    @Column(name = "grupa")
    private Integer grupa;

    @Column(name = "semigrupa")
    private Integer semigrupa;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private DidacticActivityStatus status;

    @Column(name = "week_type")
    @Enumerated(EnumType.STRING)
    private WeekType weekType;

    @Column(name = "day_of_the_week")
    @Enumerated(EnumType.STRING)
    private DayOfTheWeek dayOfTheWeek;

    @Column(name = "color")
    @Enumerated(EnumType.STRING)
    private DidacticActivityColor color;

    @Column(name = "description")
    private String description;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_tutor",referencedColumnName = "ID")
    private User tutor;

    public DidacticActivity() {
    }

    public DidacticActivity(String courseName, DidacticActivityType type, Date date, Time time, Integer duration, String room, String specialization, Integer group, Integer semigroup, DidacticActivityStatus status, WeekType weekType,  DayOfTheWeek dayOfTheWeek, DidacticActivityColor color, String description, User tutor) {
        this.courseName = courseName;
        this.type = type;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.room = room;
        this.specialization = specialization;
        this.grupa = group;
        this.semigrupa = semigroup;
        this.status = status;
        this.weekType = weekType;
        this.dayOfTheWeek = dayOfTheWeek;
        this.color = color;
        this.description = description;
        this.tutor = tutor;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public DidacticActivityType getType() {
        return type;
    }

    public void setType(DidacticActivityType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public Integer getGrupa() {
        return grupa;
    }

    public Integer getSemigrupa() {
        return semigrupa;
    }

    public void setSemigrupa(Integer semigrupa) {
        this.semigrupa = semigrupa;
    }

    public DidacticActivityStatus getStatus() {
        return status;
    }

    public void setStatus(DidacticActivityStatus status) {
        this.status = status;
    }

    public WeekType getWeekType() {
        return weekType;
    }

    public void setWeekType(WeekType weekType) {
        this.weekType = weekType;
    }

    public DayOfTheWeek getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    public void setDayOfTheWeek(DayOfTheWeek dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }

    public DidacticActivityColor getColor() {
        return color;
    }

    public void setColor(DidacticActivityColor color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getTutor() {
        return tutor;
    }

    public void setTutor(User tutor) {
        this.tutor = tutor;
    }

    public void setGrupa(Integer grupa) {
        this.grupa = grupa;
    }

    @Override
    public String toString() {
        return "DidacticActivity{" +
                "courseName='" + courseName + '\'' +
                ", type=" + type +
                ", date=" + date +
                ", time=" + time +
                ", duration=" + duration +
                ", room='" + room + '\'' +
                ", specialization='" + specialization + '\'' +
                ", grupa=" + grupa +
                ", semigrupa=" + semigrupa +
                ", tutor=" + tutor.getLastName() +
                " " + tutor.getFirstName() +
                '}';
    }
}
