package be.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author Mara Corina
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(generator="sqlite")
    @TableGenerator(name="sqlite", table="sqlite_sequence",
            pkColumnName="name", valueColumnName="seq",
            pkColumnValue="ID")
    private Integer ID;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    @Column(name = "counter")
    private Integer counter; //userul are un anumit numar de incercari gresite la autentificare

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private Integer status; //userii nu sunt stersi din baza de date, ei sunt setati ca inactivi doar din motive de persistenta

    @Column(name = "grupa")
    private Integer grupa;

    @Column(name = "semigrupa")
    private Integer semigrupa;

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getGrupa() {
        return grupa;
    }

    public void setGrupa(Integer grupa) {
        this.grupa = grupa;
    }

    public Integer getSemigrupa() {
        return semigrupa;
    }

    public void setSemigrupa(Integer semigrupa) {
        this.semigrupa = semigrupa;
    }

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    public Set<Request> getRequests() {
        return requests;
    }

    public void setRequests(Set<Request> requests) {
        this.requests = requests;
    }

    public Set<DidacticActivity> getDidacticActivities() {
        return didacticActivities;
    }

    public void setDidacticActivities(Set<DidacticActivity> didacticActivities) {
        this.didacticActivities = didacticActivities;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @OneToMany(mappedBy = "user", cascade = CascadeType.REFRESH)
    private Set<Notification> notifications = new HashSet<>();

    @OneToMany(mappedBy = "requester", cascade = CascadeType.REFRESH)
    private Set<Request> requests = new HashSet<>();

    @OneToMany(mappedBy = "tutor", cascade = CascadeType.REFRESH)
    private Set<DidacticActivity> didacticActivities = new HashSet<>();

    @ManyToMany(mappedBy = "users", cascade = CascadeType.REFRESH)
    private Set<Role> roles = new HashSet<>();

    public User() {
    }

    public User(int counter, String firstName, String lastName, String mobileNumber, String email, String username, String password, Integer status, Integer grupa, Integer semigrupa) {
        this.counter = counter;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.username = username;
        this.password = password;
        this.status = status;
        this.grupa = grupa;
        this.semigrupa = semigrupa;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID=" + ID +
                ", counter=" + counter +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", status=" + status +
                ", grupa=" + grupa +
                ", semigrupa=" + semigrupa +
                ", notifications=" + notifications +
                ", requests=" + requests +
                ", didacticActivities=" + didacticActivities +
                ", roles=" + roles +
                '}';
    }
}


