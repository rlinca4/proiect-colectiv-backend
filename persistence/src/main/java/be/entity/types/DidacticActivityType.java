package be.entity.types;

import java.util.HashMap;
import java.util.Map;

public enum DidacticActivityType {
    COURSE("COURSE"),
    SEMINARY("SEMINARY"),
    LABORATORY("LABORATORY");

    private String actualString;

    DidacticActivityType(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }

    //****** Reverse Lookup Implementation************//

    //Lookup table
    private static final Map<String, DidacticActivityType> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (DidacticActivityType didacticActivityType : DidacticActivityType.values()) {
            lookup.put(didacticActivityType.getActualString(), didacticActivityType);
        }
    }

    //This method can be used for reverse lookup purpose
    public static DidacticActivityType get(String actualString) {
        return lookup.get(actualString);
    }
}
