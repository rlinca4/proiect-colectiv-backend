package be.entity.types;

public enum WeekType {
    ODD ("ODD"),
    EVEN ("EVEN");

    private String actualString;

    WeekType(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }
}
