package be.entity.types;

public enum DidacticActivityColor {
    RED ("RED"),
    YELLOW ("YELLOW"),
    GREEN ("GREEN");

    private String actualString;

    DidacticActivityColor(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }
}
