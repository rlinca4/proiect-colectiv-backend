package be.entity.types;

import java.util.HashMap;
import java.util.Map;

public enum RequestStatus {
    PENDING("PENDING"),
    ACCEPTED("ACCEPTED"),
    DENIED("DENIED");

    private String actualString;

    RequestStatus(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }

    //****** Reverse Lookup Implementation************//

    //Lookup table
    private static final Map<String, RequestStatus> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (RequestStatus requestStatus : RequestStatus.values()) {
            lookup.put(requestStatus.getActualString(), requestStatus);
        }
    }

    //This method can be used for reverse lookup purpose
    public static RequestStatus get(String actualString) {
        return lookup.get(actualString);
    }
}
