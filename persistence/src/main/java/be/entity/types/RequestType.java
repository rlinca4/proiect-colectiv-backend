package be.entity.types;

import java.util.HashMap;
import java.util.Map;

public enum RequestType {
    REQUEST("REQUEST"),
    ACCEPT("ACCEPT"),
    DENY("DENY");

    private String actualString;

    RequestType(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }

    //****** Reverse Lookup Implementation************//

    //Lookup table
    private static final Map<String, RequestType> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (RequestType requestType : RequestType.values()) {
            lookup.put(requestType.getActualString(), requestType);
        }
    }

    //This method can be used for reverse lookup purpose
    public static RequestType get(String actualString) {
        return lookup.get(actualString);
    }
}
