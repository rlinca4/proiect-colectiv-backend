package be.entity.types;

import java.util.HashMap;
import java.util.Map;

public enum LoginType {
    SUCCES("Logat cu succes."),
    DATE_INVALIDE("Datele introduse nu sunt corecte.");
    private String actualString;

    LoginType(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }

    //****** Reverse Lookup Implementation************//

    //Lookup table
    private static final Map<String, LoginType> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (LoginType loginType : LoginType.values()) {
            lookup.put(loginType.getActualString(), loginType);
        }
    }

    //This method can be used for reverse lookup purpose
    public static LoginType get(String actualString) {
        return lookup.get(actualString);
    }

    }
