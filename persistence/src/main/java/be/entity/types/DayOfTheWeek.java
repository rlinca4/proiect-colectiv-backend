package be.entity.types;

public enum DayOfTheWeek {
    MONDAY ("MONDAY"),
    TUESDAY ("TUESDAY"),
    WEDNESDAY ("WEDNESDAY"),
    THURSDAY ("THURSDAY"),
    FRIDAY ("FRIDAY");

    private String actualString;

    DayOfTheWeek(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }
}
