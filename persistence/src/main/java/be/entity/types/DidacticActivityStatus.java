package be.entity.types;

import java.util.HashMap;
import java.util.Map;

public enum DidacticActivityStatus {
    NO_CHANGES("NO_CHANGES"),
    ALL_CHANGES("ALL_CHANGES"),
    SOME_CHANGES("SOME_CHANGES"),
    CLOSED("CLOSED");

    private String actualString;

    DidacticActivityStatus(String actualString) {
        this.actualString = actualString;
    }

    public String getActualString() {
        return actualString;
    }

    //****** Reverse Lookup Implementation************//

    //Lookup table
    private static final Map<String, DidacticActivityStatus> lookup = new HashMap<>();

    //Populate the lookup table on loading time
    static {
        for (DidacticActivityStatus didacticActivityStatus : DidacticActivityStatus.values()) {
            lookup.put(didacticActivityStatus.getActualString(), didacticActivityStatus);
        }
    }

    //This method can be used for reverse lookup purpose
    public static DidacticActivityStatus get(String actualString) {
        return lookup.get(actualString);
    }
}
