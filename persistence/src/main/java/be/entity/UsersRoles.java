package be.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users_roles")
public class UsersRoles implements Serializable {

    public UsersRoles() {
    }

    @Id
    @GeneratedValue(generator="sqlite")
    @TableGenerator(name="sqlite", table="sqlite_sequence",
            pkColumnName="name", valueColumnName="seq",
            pkColumnValue="id")
    private Integer id;

    public UsersRoles(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_user", referencedColumnName = "ID")
    private User user;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_role", referencedColumnName = "ID")
    private Role role;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
