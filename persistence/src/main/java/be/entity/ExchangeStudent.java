package be.entity;

import javax.persistence.*;

@Entity
@Table(name = "exchange_students")
@NamedQueries({
})
public class ExchangeStudent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name="ID")
    private Integer ID;

    @Column(name = "IDUser")//cine vrea sa faca schimbarea
    private Integer IDUser;

    @Column(name = "IDDidacticActivity")//cu ce sa o schimbe
    private Integer IDDidacticActivity;

    public ExchangeStudent() {
    }

    public Integer getIDUser() {
        return IDUser;
    }

    public void setIDUser(Integer IDUser) {
        this.IDUser = IDUser;
    }

    public Integer getIDDidacticActivity() {
        return IDDidacticActivity;
    }

    public void setIDDidacticActivity(Integer IDDidacticActivity) {
        this.IDDidacticActivity = IDDidacticActivity;
    }
}
