package be.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "student_activity_mapper")
public class StudentActivityMapper implements Serializable {

    @Id
    @GeneratedValue(generator="sqlite")
    @TableGenerator(name="sqlite", table="sqlite_sequence",
            pkColumnName="name", valueColumnName="seq",
            pkColumnValue="id")
    private Integer id;

    public StudentActivityMapper() {

    }

    public StudentActivityMapper(User student, DidacticActivity activity) {
        this.student = student;
        this.activity = activity;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_student", referencedColumnName = "ID")
    private User student;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="id_activity", referencedColumnName = "ID")
    private DidacticActivity activity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public DidacticActivity getActivity() {
        return activity;
    }

    public void setActivity(DidacticActivity activity) {
        this.activity = activity;
    }
}
