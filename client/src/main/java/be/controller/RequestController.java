package be.controller;

import be.dto.RequestDTO;
import be.dto.UserDTO;
import be.dtoEntityMappers.RequestDTOEntityMapper;
import be.entity.DidacticActivity;
import be.entity.Request;
import be.entity.User;
import be.entity.types.DidacticActivityStatus;
import be.exceptions.BusinessException;
import be.exceptions.ValidationException;
import be.manager.impl.DidacticActivityManager;
import be.manager.remote.RequestManagerRemote;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * REST Controller for Request manipulation.
 *
 * @author Mara Corina
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/requests")
public class RequestController {

    @Autowired
    RequestManagerRemote requestManager;

    /**
     * The Controller consumes a {@link RequestDTO} Object that
     *         maps the corresponding {@link Request} object data that
     *         will be persisted in the database.
     *
     * @return a success response containing the {@link RequestDTO} object that maps the object
     *          persisted
     * @throws {@link BusinessException} bubbles up to here from {@link RequestManagerRemote}
     *                and we return an ERROR response to the client containing the thrown exception
     */
    @RequestMapping(value = "/createRequest", method = RequestMethod.POST)
    public Response addRequest(@RequestBody RequestDTO requestDTO) {
        try {
            this.requestManager.createRequest(requestDTO);
            return Response.status(Response.Status.OK).build();
        } catch (BusinessException e) {
            return Response.status(500).build();
        } catch (ValidationException e) {
            return Response.status(500).build();
        }
    }

    /**
     * The Controller consumes a {@link Integer} Object  representing the
     *      user id  and returns a list of {@link RequestDTO} objects
     *      mapping the {@link RequestDTO} objects corresponding to activities
     *      assigned to the user with the given id persisted in the database
     * @return a success response containing the {@link RequestDTO} objects list that maps the objects
     *          persisted in the database
     * @throws {@link BusinessException} bubbles up to here from {@link RequestManagerRemote}
     *                and we return an ERROR response to the client containing the thrown exception
     */
    @GetMapping(path="/getRequests", produces = "application/json")
    public ResponseEntity<?> getAllRequests(@RequestParam Integer id) throws JsonProcessingException {
        List<RequestDTO> requestsDTOS = null;
        try {
            requestsDTOS = this.requestManager.getAllForUser(id);
            ObjectMapper jsonTransformer = new ObjectMapper();
            return ResponseEntity.ok(jsonTransformer.writeValueAsString(requestsDTOS));
        } catch (BusinessException e) {
            return ResponseEntity.badRequest().body(e);
        }
    }

}
