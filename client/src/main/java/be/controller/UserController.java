package be.controller;

import be.dto.ChangePasswordDTO;
import be.dto.NotificationDTO;
import be.dto.UserDTO;
import be.entity.User;
import be.entity.types.LoginType;
import be.exceptions.BusinessException;
import be.manager.remote.UserManagerRemote;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * REST Controller for User manipulation.
 *
 * @author Mara Corina
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/users")
public class UserController extends HttpServlet{

    @Autowired
    public UserManagerRemote userManagerRemote;

    public UserController(UserManagerRemote userManagerRemote) {
        this.userManagerRemote = userManagerRemote;
    }

    @GetMapping(path="/", produces = "application/json")
    public String get()
    {
        return "HI";
    }

    @PostMapping(path="/changePassword", produces = "application/json")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO changePasswordDTO) {
        try {
            UserDTO updateUser = userManagerRemote.changePassword(changePasswordDTO);
            return ResponseEntity.ok(updateUser);
        } catch (BusinessException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    /**
     * The Controller returns and array of {@link UserDTO} Objects that
     * map all the {@link User} objects from the database
     */
    @GetMapping(path="/getUsers", produces = "application/json")
    public String getUsers() throws IOException {
        List<UserDTO> listOfAllUsers = this.userManagerRemote.findAllUsers();

        ObjectMapper jsonTransformer = new ObjectMapper();
        return jsonTransformer.writeValueAsString(listOfAllUsers);
    }

    /**
     * The Controller consumes a {@link UserDTO} Object that
     *         maps the corresponding {@link User} object data that
     *         will be persisted in the database.
     *
     * @return a success response containing the {@link UserDTO} object that maps the object
     *          persisted
     * @throws {@link BusinessException} bubbles up to here from {@link UserManagerRemote}
     *                and we return an ERROR response to the client containing the thrown exception
     */

    @PostMapping(path="/createUser", produces = "application/json")
    public ResponseEntity<?> addUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userManagerRemote.insertUser(userDTO);
            return ResponseEntity.ok(user);
        } catch (BusinessException e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }
//
//    /**
//     * The Controller consumes a username and returns a list of {@link NotificationDTO} objects
//     * corresponding to the user with the given username from the database
//     *
//     * @param username is a {@link String}
//     * @return a success response containing a {@link NotificationDTO} object array
//     * @throws {@link BusinessException} bubbles up to here from {@link UserManagerRemote}
//     *                and we return an ERROR response to the client containing the thrown exception
//     * @author Mara Corina
//     */
    @GET
    @Path("/{username}/notifications")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserNotifications(@PathParam("username") String username) throws IOException, BusinessException {
        Set<NotificationDTO> listOfNotifications = userManagerRemote.getUserNotifications(username);

        ObjectMapper jsonTransformer = new ObjectMapper();
        return jsonTransformer.writeValueAsString(listOfNotifications);
    }


    /**
     * The Controller consumes an id and returns an {@link UserDTO} Object that
     * maps the corresponding {@link User} object data persisted in the database.
     *
     * @param id is an {@link Integer}
     * @return a success response containing the {@link UserDTO} object that maps the object
     * from teh database
     * @throws {@link BusinessException} bubbles up to here from {@link UserManagerRemote}
     *                and we return an ERROR response to the client containing the thrown exception
     */
    @GetMapping(path="/getUser/{id}", produces = "application/json")
    public String getUser(@PathVariable("id") Integer id) {
        try {
            UserDTO userDTO = userManagerRemote.findUser(id);
            ObjectMapper jsonTransformer = new ObjectMapper();
            return jsonTransformer.writeValueAsString(userDTO);
        } catch (BusinessException | JsonProcessingException e) {
            // Obs: throw new UserNotFoundException - Status 404
            return null;
        }
    }

    /**
     * The Controller returns a list of Integers
     *         representing all the {@link User} groups
     *         persisted in the database.
     *
     * @return a success response containing the Integers list
     */
    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public ResponseEntity<?> getGroups() {
        List<Integer> groups = this.userManagerRemote.getAllGroups();
        return ResponseEntity.ok(groups);
    }
}
