package be.controller;

import be.dto.DidacticActivityDTO;
import be.dto.StudentActivityMapperDTO;
import be.dto.UserDTO;
import be.entity.DidacticActivity;
import be.entity.User;
import be.entity.Utils.StudentDidacticRequest;
import be.exceptions.BusinessException;
import be.manager.remote.DidacticActivityRemote;
import be.manager.remote.StudentDidacticRemote;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.List;

@RestController
@RequestMapping(path = "/student")
public class StudentDidacticController {
    @Autowired
    public StudentDidacticRemote studentDidacticRemote;

    @Autowired
    public DidacticActivityRemote didacticActivityRemote;

    @PostMapping(path="/getActivities", produces = "application/json")
    public String getAllActivities(@RequestBody UserDTO userDTO) throws JsonProcessingException {
        List<DidacticActivityDTO> didacticDTOS = this.studentDidacticRemote.getAllStudentDidacticActivities(userDTO.getID());

        ObjectMapper jsonTransformer = new ObjectMapper();
        return jsonTransformer.writeValueAsString(didacticDTOS);
    }

    @PostMapping(path="/getActivitiesForChange", produces = "application/json")
    public String getAllActivities(@RequestBody DidacticActivityDTO didacticActivityDTO) throws JsonProcessingException {
        List<DidacticActivityDTO> didacticDTOS = this.didacticActivityRemote.getAllActivitiesForChange(didacticActivityDTO);

        ObjectMapper jsonTransformer = new ObjectMapper();
        return jsonTransformer.writeValueAsString(didacticDTOS);
    }

    /**
     * The Controller consumes a userId, an activityId and returns an {@link StudentActivityMapperDTO} Object that
     * maps the corresponding {@link User} object data persisted in the database and
     * the corresponding {@link DidacticActivity} object data persisted in the database.
     *
     * @return a success response
     * @throws {@link BusinessException} bubbles up to here from {@link DidacticActivityRemote}
     *                and we return an ERROR response to the client containing the thrown exception
     */
    @PostMapping(path="/createStudentActivity", produces = "application/json")
    public Response addStudentDidacticActivity(@RequestBody StudentDidacticRequest request) {
        try {
            StudentActivityMapperDTO studentActivityMapperDTO = this.studentDidacticRemote.insertUser(request.getUserId(), request.getActivityId());
            return Response.status(Response.Status.OK).build();
        } catch (BusinessException e) {
            return Response.status(500).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }

    @PostMapping(path="/deleteStudentActivity", produces = "application/json")
    public Response deleteStudentDidacticActivity(@RequestBody StudentDidacticRequest request) {
        try {
            StudentActivityMapperDTO studentActivityMapperDTO = this.studentDidacticRemote.deleteActivitiesForUser(request.getUserId(), request.getActivityId());
            return Response.status(Response.Status.OK).build();
        } catch (BusinessException e) {
            return Response.status(500).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).build();
        }
    }
}
