package be.controller;

import be.dto.DidacticActivityDTO;
import be.entity.DidacticActivity;
import be.manager.remote.DidacticActivityRemote;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/didacticActivity")
public class DidacticActivityController {

    @Autowired
    public DidacticActivityRemote didacticActivityRemote;

    @PostMapping(path="/update", produces = "application/json")
    public ResponseEntity<?> updateDidacticActivity(@RequestBody DidacticActivityDTO didacticActivityDTO) {
        DidacticActivity didacticActivity = this.didacticActivityRemote.update(didacticActivityDTO);
        //Recursive problem
        didacticActivity.setTutor(null);
        return ResponseEntity.ok(didacticActivity);
    }

    @GetMapping(path="/getAllCourses", produces = "application/json")
    public String getAllCourses() throws JsonProcessingException {
        ObjectMapper jsonTransformer = new ObjectMapper();
        return jsonTransformer.writeValueAsString(this.didacticActivityRemote.getAllCourses());
    }
}
