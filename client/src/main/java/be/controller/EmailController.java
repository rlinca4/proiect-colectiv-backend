package be.controller;

import be.dto.EmailDTO;
import be.exceptions.BusinessException;
import be.manager.impl.email.EmailManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.ws.rs.core.Response;

@RestController
@RequestMapping(path = "/email")
public class EmailController {

    @Autowired
    public EmailManager emailService;

    @PostMapping(path="/sendEmail", produces = "application/json")
    public Response sendEmail(@RequestBody EmailDTO emailDTO) {
          try {
              emailService.sendSimpleMessage(emailDTO);
              return Response.status(200).build();
          } catch (BusinessException ex) {
              return Response.status(400).build();
          }
    }
}
