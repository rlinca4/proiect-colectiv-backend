package be.controller;

import be.dto.TeacherDidacticDTO;
import be.dto.UserDTO;
import be.manager.remote.TeacherDidacticRemote;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/teacher")
public class TeacherDidacticController {
    @Autowired
    public TeacherDidacticRemote teacherDidacticRemote;

    @PostMapping(path="/getActivities", produces = "application/json")
    public String getAllActivities(@RequestBody UserDTO userDTO) throws JsonProcessingException {
        List<TeacherDidacticDTO> didacticDTOS = this.teacherDidacticRemote.getAllTeacherDidacticActivities(userDTO.getID());

        ObjectMapper jsonTransformer = new ObjectMapper();
        return jsonTransformer.writeValueAsString(didacticDTOS);
    }
}
