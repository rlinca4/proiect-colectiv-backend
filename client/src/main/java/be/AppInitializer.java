package be;

import be.parser.Timetable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import java.io.IOException;

@SpringBootApplication
@ComponentScan(basePackages = {"be.*"})
public class AppInitializer {
    @Autowired
    Timetable timetable;

    public static void main(String[] args) throws IOException {
        SpringApplication.run(AppInitializer.class, args);
    }

//    @PostConstruct
//    public void Init(){
//        try {
//            //timetable.createTeacherAccounts();
//            timetable.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
